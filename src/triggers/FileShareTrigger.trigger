trigger FileShareTrigger on File_Sharing__c (after insert, after update) {
    EmailManager em = new EmailManager();  
    for(File_Sharing__c FS : Trigger.New){ 
        try{ 
            system.debug('==Inside Try==');
            String toEmail = [SELECT User_Email__c from User_Management__c WHERE Id =: FS.Tenant_User__c].User_Email__c ;
            String ccEmail = [SELECT User_Email__c from User_Management__c WHERE Id =: FS.CreatedBy__c].User_Email__c ;
            List<String> CcAddresses = new List<String>();
            CcAddresses.add(system.label.TradeAix_email_ID);      
            CcAddresses.add(ccEmail);      
            String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'DocumentVaultEmailTemplate' LIMIT 1].Id;
            em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(FS.Id));
        }catch(exception ex){
            system.debug('===File sharing trigger exception==='+ex.getmessage());
        }
    }
    
}