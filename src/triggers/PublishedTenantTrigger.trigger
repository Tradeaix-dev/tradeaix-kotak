trigger PublishedTenantTrigger on Published_Tenant__c (before insert,after update) {
EmailManager em = new EmailManager();  
try{
for (Published_Tenant__c AL: Trigger.new) {


 if(Trigger.isAfter){
 if(AL.Declined__c){ 
    List<String> CcAddresses = new List<String>(); 
    system.debug('======isInsert=====');    
    Published_Tenant_User__c PT =[SELECT id,Tenant_User__c FROM Published_Tenant_User__c WHERE Published_Tenant__c=:AL.ID Limit 1]; 
    Transaction__c t = [Select Id, Name,CreatedBy__c,TransactionRefNumber__c,status__c from Transaction__c where Id = :AL.Transaction__c];
              
    String toEmail1 = [SELECT User_Email__c from User_Management__c WHERE id=:t.CreatedBy__c].User_Email__c ;
    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: PT.Tenant_User__c];                 
    for( User_Management__c lstStr: listUserEmail){
    CcAddresses.add(lstStr.User_Email__c) ;
    }
    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'QuoteDeclineEmailTemp' LIMIT 1].Id;
    em.sendMailWithTemplate(toEmail1 , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(AL.Id));
    
    
    }
    }else{
    TimeZone tz = UserInfo.getTimeZone();
AL.CreatedDatePDF__c = System.Now().format('dd/MM/YYYY HH:mm:ss', tz.getID());
}
}
}catch(exception ex){
system.debug('=======Exception======='+ex.getMessage());
}
}