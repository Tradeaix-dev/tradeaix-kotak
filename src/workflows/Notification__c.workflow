<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AmendRequestEmailAlert</fullName>
        <ccEmails>gaurav.regekmb@gmail.com</ccEmails>
        <ccEmails>nabtropsau@gmail.com</ccEmails>
        <ccEmails>dbtropspac@gmail.com</ccEmails>
        <description>AmendRequestEmailAlert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AmendRequestEmail</template>
    </alerts>
    <rules>
        <fullName>AmendRequestRule</fullName>
        <actions>
            <name>AmendRequestEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Notification__c.Action__c</field>
            <operation>equals</operation>
            <value>Amendment Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
