<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Attribute_Value_Short</fullName>
        <field>Attribute_Value_Short__c</field>
        <formula>LEFT( Attribute_Value__c ,255)</formula>
        <name>Update Attribute Value Short</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
