public class LightLoginAccountsV {
    @AuraEnabled
    public String name = '';
    @AuraEnabled
    public String accountId = '';
    @AuraEnabled
    public String baseUrl = '';
    @AuraEnabled 
    public String isDefault = '';
    @AuraEnabled
    public String userName = '';
    @AuraEnabled
    public String userId = ''; 
    @AuraEnabled
    public String email = '';
    @AuraEnabled
    public String siteDescription = '';  
}