public class QuoteDeclineEmailTempctrl{
    public Id tenantuserId {get;set;} 
    
    public class UserDetails { 
    public String userId {get;set;}
    public String username {get;set;}
    public String sitename {get;set;}  
    public String Thanksmsg{get;set;}
    public String logo{get;set;}
    public String logotitle{get;set;} 
    public String Transref{get;set;} 
    public String Biddername{get;set;}
    public String reason{get;set;}
    
    }
    
    public UserDetails email {get;set;}  
    public UserDetails getUsers()
    {
    email = new UserDetails();
    try{
    Published_Tenant__c UM = [SELECT Id,Reject_Reason__c,Transaction_RefNumber__c from Published_Tenant__c WHERE Id=:tenantuserId];
    Published_Tenant_User__c PT =[SELECT id,Tenant_User__c FROM Published_Tenant_User__c WHERE Published_Tenant__c=:UM.ID Limit 1];
    system.debug('##'+UM);
    User_Management__c UM1 = [SELECT Id,First_Name__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c FROM User_Management__c WHERE id=:PT.Tenant_User__c];
    system.debug('##'+UM1);
    User_Management__c UM2 = [SELECT Id,First_Name__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c FROM User_Management__c WHERE id=:UM1.CreatedBy__c];
    
    
    email.reason=UM.Reject_Reason__c;
    
    email.Biddername= UM1.First_Name__c+' '+UM1.Last_Name__c;
    email.username = UM2.First_Name__c+' '+UM2.Last_Name__c;
    email.sitename = UM2.Tenant__r.Tenant_Site_Name__c;
    email.logo = UM1.Tenant__r.Tenant_Logo_Url__c;
    email.Thanksmsg= UM1.Tenant__r.Tenant_Footer_Message__c;
    
    email.Transref = UM.Transaction_RefNumber__c;
    email.userId = UM1.Id;
    }catch(exception ex){
    system.debug('//****// '+ex.getMessage());
    }
    return email;
    }
    }