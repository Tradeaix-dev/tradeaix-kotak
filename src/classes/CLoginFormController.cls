global with sharing class CLoginFormController {

    public CLoginFormController() {
        
    }   
    @AuraEnabled
    public static String Customlogin(String username, String password, String startUrl) {
        try{
            system.debug('<<0<<INsideLogin');
            string sessionId = getSessionId();
            //return sessionId;
            system.debug('<<1<<'+sessionId);
            HTTP h = new HTTP();
            system.debug('<<2<<'+h);
            HTTPRequest req = new HTTPRequest();
            system.debug('<<3<<'+req);
            HttpResponse resp = new HttpResponse();
            system.debug('<<4<<'+resp);
            req.setMethod('GET');
            req.setHeader('Authorization', 'Bearer ' + sessionId);
            string EndpointURL =System.Label.getURL+username+'&'+password;
            system.debug('<<5<<'+EndpointURL); 
            req.setEndpoint(EndpointURL);
            system.debug('<<6<<'+req);
            resp = h.send(req);
            system.debug('<<7<<'+resp);
            
            //LightningLoginFormController.login('jindal_manager@gmail.com.tradeaix99','bank@12345', 'tradaix/s/tradedashboard/');
            
            return resp.getBody();
            
        }
        catch (Exception ex) {
            system.debug('<<Catch<<');
            return ex.getMessage();            
        }
    }
 
    private static string getSessionId(){
        String retValue = '';
        //try{
            partnerSoapSforceCom.Soap sp = new partnerSoapSforceCom.Soap();
            partnerSoapSforceCom.LoginResult loginResult = sp.login(
                'ssonalkar1@stratizant.com.tradeaix99',
                'Password@123453IRZuImBWKqNS9ceWfbdy2o7M'
            );
            retValue = loginResult.sessionId;
            system.debug('<<retValue<<'+retValue);
            //partnerSoapSforceCom.SessionHeader_element session =new partnerSoapSforceCom.SessionHeader_element();
            //session.sessionId = LoginResult.sessionId;
        //} catch(Exception ex){
          //  throw new AppException('getImpersonateSessionId() - ' + ex.getMessage());
        //}
        return retValue;
    }

    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }

    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }

    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }

    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }
}