public with sharing class LightOrganizationNewCtrl {
    
    
    @AuraEnabled
    public static Organization__c[] GetOrg()
    {
        return [SELECT Id, Name from Organization__c];
    }
    @AuraEnabled
    public static string SaveTenant(String LoggedUserID,String OrgID,Tenant__c TenantDetails)
    {
        String retvalue='';
        try{
            TenantDetails.Organization__c =OrgID;
            TenantDetails.CreatedBy__c=LoggedUserID; 
            TenantDetails.LastModifiedBy__c=LoggedUserID;
            //TenantDetails.CreatedFromOTP__c =true;
            Insert TenantDetails;
            User_Management__c[] UserName =[SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            Helper.ActivityLogInsertCallForRecord(LoggedUserID,TenantDetails.Id,'New Tenant',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New Tenant - '+TenantDetails.Tenant_Site_Name__c,false);
        }    
        catch(Exception ex){
            System.debug('SaveTenant: ' + ex.getMessage());
        }
        Return retvalue; 
    }
    @AuraEnabled
    public static string SavesetupOrg(String LoggedUserID,Organization__c OrgDetails,Tenant__c TenantDetails,User_Management__c TenantUserDetails,Boolean iterationcall,String InsertedTIds)
    {
        String retvalue='';
        try{
            User_Management__c[] UserName =[SELECT First_Name__c,Last_Name__c FROM User_Management__c WHERE id=:LoggedUserID Limit 1];
            if(!iterationcall){
                OrgDetails.CreatedBy__c=LoggedUserID;
                OrgDetails.LastModifiedBy__c=LoggedUserID;
                //OrgDetails.CreatedFromOTP__c=true;
                Insert OrgDetails;
            
             Helper.ActivityLogInsertCallForRecord(LoggedUserID,OrgDetails.id,'New Organization',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New Organization - '+OrgDetails.Name,false);
            
            system.debug('##ID##'+OrgDetails.Id);
            if(OrgDetails.Id!=null){
                TenantDetails.Organization__c =OrgDetails.Id;
                TenantDetails.CreatedBy__c=LoggedUserID;
                TenantDetails.LastModifiedBy__c=LoggedUserID;
                TenantDetails.Tenant_Short_Name__c = TenantDetails.Tenant_Short_Name__c.toLowerCase();
                //TenantDetails.CreatedFromOTP__c =true;
                Insert TenantDetails;
                
                if(TenantDetails.Tenant_Type__c=='Corporate')
                { 
                    Organization__c Org = [SELECT Id FROM Organization__c WHERE Id=:OrgDetails.Id LIMIT 1];
                    Org.isPrimary__c=true;
                    Update Org;
                }
                Helper.ActivityLogInsertCallForRecord(LoggedUserID,TenantDetails.id,'New Tenant',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New Tenant - '+TenantDetails.Tenant_Site_Name__c,false);

                system.debug('##TenantDetails##'+TenantDetails.Id);
                if(TenantDetails.Id!=null){
                    TenantUserDetails.Tenant__c = TenantDetails.Id;
                    TenantUserDetails.Active__c =true;
                string profilename = '';
                if(TenantUserDetails.User_Profile__c == 'Party Inputter' || TenantUserDetails.User_Profile__c == 'Party Approver'){
                    profilename = 'Seller Associate Community Plus';
                }
                if(TenantUserDetails.User_Profile__c == 'Party Validator'){
                    profilename = 'Seller Validator Community Plus';
                }
                if(TenantUserDetails.User_Profile__c == 'Counterparty Validator'){
                    profilename = 'Buyer Validator Community Plus';
                }
                  if(TenantUserDetails.User_Profile__c == 'Counterparty Manager'){
                    profilename = 'Buyer Manager Community Plus';
                }
                if(TenantUserDetails.User_Profile__c == 'Party Manager'){
                    profilename = 'Seller Manager Community Plus';
                }
                    if(TenantUserDetails.User_Profile__c == 'Party Admin'){
                    profilename = 'Seller Admin Community Plus';
                }
                system.debug('===Profile Name==='+profilename);
                    List<Profiles__c> profileId = [Select Id,Name from Profiles__c WHERE Name =: profilename LIMIT 1];
                    TenantUserDetails.Profiles__c=profileId[0].Id;
                    TenantUserDetails.CreatedBy__c=LoggedUserID;
                    TenantUserDetails.LastModifiedBy__c=LoggedUserID;
                    Insert TenantUserDetails;
                    Helper.ActivityLogInsertCallForRecord(LoggedUserID,TenantUserDetails.id,'New Tenant User',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New User - '+TenantUserDetails.First_Name__c+' '+TenantUserDetails.Last_Name__c,false);
                }
            }
            }else{
                TenantUserDetails.Tenant__c = InsertedTIds;
                TenantUserDetails.Active__c =true;
                string profilename = '';
                if(TenantUserDetails.User_Profile__c == 'Party Inputter' || TenantUserDetails.User_Profile__c == 'Party Approver'){
                    profilename = 'Seller Associate Community Plus';
                }
                if(TenantUserDetails.User_Profile__c == 'Party Validator'){
                    profilename = 'Seller Validator Community Plus';
                }
                if(TenantUserDetails.User_Profile__c == 'Counterparty Validator'){
                    profilename = 'Buyer Validator Community Plus';
                }
                  if(TenantUserDetails.User_Profile__c == 'Counterparty Manager'){
                    profilename = 'Buyer Manager Community Plus';
                }
                if(TenantUserDetails.User_Profile__c == 'Party Manager'){
                    profilename = 'Seller Manager Community Plus';
                }
                    if(TenantUserDetails.User_Profile__c == 'Party Admin'){
                    profilename = 'Seller Admin Community Plus';
                }
                system.debug('===Profile Name==='+profilename);
                List<Profiles__c> profileId = [Select Id,Name from Profiles__c WHERE Name =: profilename LIMIT 1];
                TenantUserDetails.Profiles__c=profileId[0].Id;                     
                TenantUserDetails.CreatedBy__c=LoggedUserID;
                TenantUserDetails.LastModifiedBy__c=LoggedUserID;
                Insert TenantUserDetails;
                Helper.ActivityLogInsertCallForRecord(LoggedUserID,TenantUserDetails.id,'New Tenant User',UserName[0].First_Name__c+' '+UserName[0].Last_Name__c+' is Created New User - '+TenantUserDetails.First_Name__c+' '+TenantUserDetails.Last_Name__c,false);
               
            }
        }    
        catch(Exception ex){
            System.debug('SavesetupOrg: ' + ex.getMessage());
        }
        Return TenantDetails.Id; 
    }
    
    @AuraEnabled
    public static String CheckOrgName(String OrgName) { 
        String response = '';
        try {  
            Integer count = [SELECT count() FROM Organization__c WHERE Name = : OrgName]; 
            if(count > 0){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('Organization - CheckOrgName() : ' + ex.getMessage());
            response = '';
        }
        return response; 
    } 
     @AuraEnabled
    public static LightOrganizationNewCtrlV1 CheckTenantName(String TenantName,String Tenantsiteval) { 
        system.debug('Tenantnameval'+TenantName);
        system.debug('Tenantsiteval'+Tenantsiteval);
        LightOrganizationNewCtrlV1 LC =NEW LightOrganizationNewCtrlV1();
        try {  
            Integer count = [SELECT count() FROM Tenant__c WHERE Tenant_Site_Name__c = : TenantName]; 
             system.debug('count'+count);
            if(count > 0){
                LC.CheckTenantUserName = 'Already Used';
            }
            else { 
                LC.CheckTenantUserName = '';
            }  
            
            Integer count1 = [SELECT count() FROM Tenant__c WHERE Tenant_Short_Name__c = : Tenantsiteval]; 
           system.debug('count1'+count1);
            if(count1 > 0){
                LC.CheckTenantsiteName = 'Already Used';
            }
            else { 
                LC.CheckTenantsiteName = '';
            }  
            
        } catch(Exception ex){
            System.debug('Tenant - CheckTenantName() : ' + ex.getMessage());
        }
        return LC; 
    } 
    @AuraEnabled
    public static String CheckTenantUserName(String UserName) { 
        String response = '';
        try {  
            Integer count = [SELECT count() FROM User_Management__c WHERE User_Name__c = : UserName]; 
            if(count > 0){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('Tenant User - CheckTenantUserName() : ' + ex.getMessage());
            response = '';
        }
        return response; 
    } 
    
}