public class TenantCreateEmailTemp {
	public Id tenantId {get;set;} 
    public class tenantDetails {
        public String tenantId {get;set;}
        public String tenantname {get;set;}
        public String orgname {get;set;}
        public String swiftcode {get;set;}
        public String tenanttype {get;set;}
        public String contactname {get;set;}
        public String contactphone {get;set;}
        public String contactemail {get;set;}
        public String street {get;set;}
        public String city {get;set;}
        public String state {get;set;}
        public String zip {get;set;}
        public String country {get;set;}
        public String approverejectreason {get;set;}
        public String bankstatus {get;set;}
        public String Thanksmsg{get;set;}
        public String logo{get;set;}
        public String tradeaixlogo{get;set;}
        public String Id {get;set;}
        public String userTenantname {get;set;}
        public String CreatedBy {get;set;}
        public String CreatedDate {get;set;}
        public String adminname {get;set;}
    }
    public tenantDetails email {get;set;}  
    public tenantDetails getTenant()
    {
        email = New tenantDetails();
        try{
            Tenant__c T = [SELECT Id, Name, Tenant_Site_Name__c, Organization__r.Name, Tenant_Swift_Code__c, Tenant_Type__c,Primary_Contact_Email__c,Primary_Contact_Mobile__c,Primary_Contact_Name__c,Tenant_Logo_Url__c,Tenant_Footer_Message__c,CreatedBy__c,CreatedDate,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,Street__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c,Approve_Reject_Reason__c,CreatedBy__r.Tenant__r.Tenant_Site_Name__c,Issuing_Bank_Approved__c,Issuing_Bank_Status__c,CreatedBy__r.Tenant__r.Tenant_Logo_Url__c,CreatedBy__r.Tenant__r.Tenant_Footer_Message__c,CreatedBy__r.User_Title__c from Tenant__c WHERE Id=:tenantId];
            email.tenantname = T.Tenant_Site_Name__c;
            email.orgname = T.Organization__r.Name;
            email.swiftcode = T.Tenant_Swift_Code__c;
            email.tenanttype = T.Tenant_Type__c;
            email.contactname = T.Primary_Contact_Name__c;
            email.contactphone = T.Primary_Contact_Mobile__c;
            email.contactemail = T.Primary_Contact_Email__c;
            email.logo = T.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c;
            if(T.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c == null || T.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c == ''){            
                 email.logo =system.label.TradeAix_Logo;
             }
            email.street = T.Street__c;
            email.city = T.City__c;
            email.state = T.State_Province__c;
            email.zip = T.Zip_Postal_Code__c;
            email.country = T.Country__c;
            email.Thanksmsg = T.CreatedBy__r.Tenant__r.Tenant_Footer_Message__c;
            email.approverejectreason = T.Approve_Reject_Reason__c;
            email.Id = T.Id;
            email.userTenantname = T.CreatedBy__r.Tenant__r.Tenant_Site_Name__c; 
            email.bankstatus = T.Issuing_Bank_Status__c;
            Datetime myDT = T.CreatedDate; 
            TimeZone tz = UserInfo.getTimeZone();
            String myDate = myDT.format('dd/MM/YYYY HH:mm:ss', tz.getID()); 
            email.CreatedDate = myDate;
            if(T.CreatedBy__r.User_Title__c != null){
                email.CreatedBy = T.CreatedBy__r.User_Title__c+' '+T.CreatedBy__r.First_Name__c+' '+T.CreatedBy__r.Last_Name__c; 
            }else{
                email.CreatedBy = T.CreatedBy__r.First_Name__c+' '+T.CreatedBy__r.Last_Name__c; 
            }            
            User_Management__c UM = [SELECT Id, Name,User_Title__c,First_Name__c,Last_Name__c from User_Management__c WHERE Id =: system.label.Admin_TradeAix.split(';')[1]];
        	email.adminname = UM.User_Title__c+' '+UM.First_Name__c+' '+UM.Last_Name__c;
            email.tradeaixlogo = system.label.TradeAix_Logo;
        }catch(exception e){
            system.debug('//****// '+e.getMessage());
        }
        return email;
    }
}