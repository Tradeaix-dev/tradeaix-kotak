public class LightViewTenantUsersClsV {
    @AuraEnabled 
    public List<User_Management__c> userslist = new List<User_Management__c>();
    @AuraEnabled 
    public List<String> idString = new List<String>();
}