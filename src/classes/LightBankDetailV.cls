public class LightBankDetailV {
    @AuraEnabled
    public Tenant__c result = new Tenant__c(); 
    @AuraEnabled
    public String Platformmsg {get; set;}
    @AuraEnabled
    public String Bankmsg {get; set;}
    @AuraEnabled
    public String Attachmentid {get; set;}
    @AuraEnabled
    public List<Tenant_Mapping__c> TM = NEW List<Tenant_Mapping__c>();
    @AuraEnabled
    public List<Activity_Log__c> Activity_Log = NEW List<Activity_Log__c>(); 
    @AuraEnabled
    public List<Tenant_Mapping__c> TList = new List<Tenant_Mapping__c>();
    @AuraEnabled
    public List<User_Management__c> userResults {get;set;}
}