public class IssuingBankCtrlV {
    @AuraEnabled
    public Tenant__c result = new Tenant__c(); 
    @AuraEnabled
    public List <Activity_Log__c> Alogs = new List <Activity_Log__c>();
}