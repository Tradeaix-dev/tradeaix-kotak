public class LightSuperUserDashboardHeaderV {
    //@AuraEnabled 
    //public List <LightOfferExtended> results = new List <LightOfferExtended>();
    @AuraEnabled
    public List <Transaction__c> TranspastList = new List <Transaction__c>(); 
    @AuraEnabled
    public List <Transaction__c> TranstodayList = new List <Transaction__c>();
    @AuraEnabled
    public List <Tenant__c> TenantList = new List <Tenant__c>();
    @AuraEnabled //Total # of registered CUs
    public Integer TotalNoOfProductTemplates = 0;
    @AuraEnabled //Total # of registered CUs
    public Integer TotalNoOFPartyBank = 0; 
    @AuraEnabled //Total # of registered CUs
    public Integer TotalNoOfCounterPartyBanks = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer NoOfTransactions = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfCompletedTransactions = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfInactiveTransactions = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfBids = 0;
    @AuraEnabled //% of users active in past 0-7 days
    public Integer PcOfUsers_Active = 0;
    @AuraEnabled 
    public String BankMSg = '';
    @AuraEnabled 
    public String BankMsglink = '';
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfOrg = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfTenant = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfTenantUser = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfTransactions = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfCorp = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfCP = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TotalNoOfatt = 0;
    
    @AuraEnabled //Total # of Transactions 
    public Integer TCCount = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TRFICount = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TRFQCount = 0;
    @AuraEnabled //Total # of Transactions 
    public Integer TACount = 0;
    
    
}