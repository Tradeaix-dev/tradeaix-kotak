public class LightUserProfileV {
    @AuraEnabled
    public User_Management__c Userlists {get;set;}
    @AuraEnabled
    public List<User_Management__c> userResults {get;set;}
    @AuraEnabled
    public List<User_Management__c> userResults1 {get;set;}
    @AuraEnabled
    public Boolean isAdmin{get;set;}
    @AuraEnabled 
    public List<Activity_Log__c> Activity_Log = new List<Activity_Log__c> ();
    @AuraEnabled 
    public List<Login_History__c> LoginHistory = new List<Login_History__c> ();
    @AuraEnabled
    public List<Organization__c> orgLists {get;set;}
    @AuraEnabled
    public List<Tenant__c> tenantLists {get;set;}
    @AuraEnabled
    public Boolean tradeaixAdmin{get;set;}
    @AuraEnabled
    public List<User_Notification__c> usernotificationLists {get;set;}
    @AuraEnabled
    public List<String> countrycodelist {get;set;}
}