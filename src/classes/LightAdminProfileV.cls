public class LightAdminProfileV {
    @AuraEnabled
    public Organization__c organization {get;set;}
    @AuraEnabled
    public Tenant__c tenant {get;set;}
    @AuraEnabled
    public List<Organization__c> orgLists {get;set;}
    @AuraEnabled
    public List<Tenant__c> tenantLists {get;set;}
    @AuraEnabled
    public List<User_Management__c> userLists {get;set;}
    @AuraEnabled 
    public List<Activity_Log__c> Activity_Log = new List<Activity_Log__c> ();
    @AuraEnabled
    public List<Tenant_Mapping__c> TMCP = NEW List<Tenant_Mapping__c>();
    @AuraEnabled
    public List<Tenant_Mapping__c> TMIB = NEW List<Tenant_Mapping__c>();
}