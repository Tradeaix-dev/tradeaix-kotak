public class LightBuyerBidListV {
    @AuraEnabled
    public List <Quotes__c> results = new List <Quotes__c>();
    @AuraEnabled
    public Integer counter = 0; 
    @AuraEnabled
    public String selectedItem = ''; 
    @AuraEnabled
    public String showpage = '';
    @AuraEnabled
    public String sortbyField = '';
    @AuraEnabled
    public String sortDirection = '';
    @AuraEnabled
    public Integer total_page = 0; 
    @AuraEnabled
    public Integer list_size = 0;
    @AuraEnabled
    public Integer total_size = 0;      
    @AuraEnabled
    public Boolean isFilterA = false;
    @AuraEnabled
    public UI_Filter__c filter = new UI_Filter__c();
}