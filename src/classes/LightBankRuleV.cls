public class LightBankRuleV {
    @AuraEnabled
    public Tenant__c bank = new Tenant__c(); 
    
    @AuraEnabled
    public List<User_Management__c> CUUsersInp = new List<User_Management__c>();
    @AuraEnabled
    public List<User_Management__c> CUUsersval = new List<User_Management__c>();
    @AuraEnabled
    public List<User_Management__c> CUUsersapp1 = new List<User_Management__c>();
    @AuraEnabled
    public List<User_Management__c> CUUsersapp2 = new List<User_Management__c>();
    @AuraEnabled
    public List<User_Management__c> CUUsersman = new List<User_Management__c>();
    @AuraEnabled
    public List<User_Management__c> CUUsersCPval = new List<User_Management__c>();
    @AuraEnabled
    public List<User_Management__c> CUUsersCPbid = new List<User_Management__c>();
     @AuraEnabled
    public List<User_Management__c> CUUserscom = new List<User_Management__c>();
    @AuraEnabled 
    public List<BankFields> ComSourceFields {get;set;}
    @AuraEnabled 
    public List<String> ComSelectedFields {get;set;}
    @AuraEnabled 
    public List<BankFields> ValSourceFields {get;set;}
    @AuraEnabled 
    public List<String> ValSelectedFields {get;set;}
    @AuraEnabled 
    public List<BankFields> AppSourceFields {get;set;}
    @AuraEnabled 
    public List<String> AppSelectedFields {get;set;}
    
    @AuraEnabled 
    public integer CountPendval {get;set;}
    @AuraEnabled 
    public integer CountPendApp {get;set;}
    @AuraEnabled 
    public integer CountPendRFI {get;set;}
    @AuraEnabled 
    public integer CountPendRFQ {get;set;}
}