public with sharing class LightIBanksList {
    public static String query = ''; 
    public static Integer size = 0;
     @AuraEnabled 
    public static Organization__c[] loadorgs(String username) {
        return Helper.loadorgs(username);
    }
    @AuraEnabled
    public static String getloggeduserBrowserTitle() { 
        /*String Btitle = '';
        try {  
            List<Credit_Union_User__c> lstUsers = [SELECT Credit_Union__c,User__c FROM Credit_Union_User__c WHERE User__c =:userinfo.getUserId()];
            Btitle = [SELECT Browser_Title__c FROM Credit_Union__c WHERE Id =: lstUsers[0].Credit_Union__c].Browser_Title__c;
        }
        catch (Exception e) { 
            system.debug('ERROR ===== '+e.getMessage());
        }*/
        return '';
    }
     @AuraEnabled
    public static LightBankListV Filter(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        //Helper.setSessionValue('sessionselected', selected);
        return ShowTable(counter, sortbyField, sortDirection, selected,Filtervalue,TID); 
    }
    @AuraEnabled
    public static string addCounterPartiesname(){
        String retValue = '';
        
        return retValue;
    }
    private static LightBankListV ShowTable(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID){ 
        LightBankListV retValue = new LightBankListV();
        try{
            String sessionselectedchk='';
            if((String)Cache.Session.get('sessionselected') != null)  
                sessionselectedchk= Helper.getSessionValue('sessionselected');  
            system.debug('#sessionselected#'+sessionselectedchk);
            String whereCondition = ''; 
            if(sessionselectedchk == 'Active' || sessionselectedchk == ''){
                whereCondition = ' WHERE Tenant_Approved__r.IsActive__c = True';
                
            }
            else if(sessionselectedchk == 'All'){
                whereCondition = ' WHERE (Tenant_Approved__r.IsActive__c = True OR Tenant_Approved__r.IsActive__c = false)';
            }
            else if(sessionselectedchk == 'Inactive'){
                whereCondition = ' WHERE Tenant_Approved__r.IsActive__c = false';
            }
            if(Filtervalue!='All Realms' && Filtervalue!=null)
            {
                String setTids='(';
                if(Filtervalue.startsWith('a0M')){
                    
                    for(Tenant__c T:[SELECT id FROM Tenant__c WHERE Organization__c=:Filtervalue])
                    {
                       setTids =setTids +'\''+T.id+'\',';                        
                    }
                    setTids =setTids+')';
                    if(setTids != '(')
                    {
                        system.debug('##'+setTids);
                     String FinalsetTids = setTids.replace(',)', ')');
                        system.debug('#FinalsetTids#'+FinalsetTids);
                     whereCondition = whereCondition + ' AND id IN'+FinalsetTids;
                        
                    }
                }else{
                whereCondition = whereCondition + ' AND id=\''+Filtervalue+'\'';
                }
             }
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList']; 
            
            if(size > 0){
                retValue.list_size = Integer.valueOf([SELECT List_Size__c from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList'][0].List_Size__c);
            }
            else {
                retValue.list_size = 25;   
            }
            
            String query = 'SELECT Tenant__c,Tenant__r.CreatedBy__c,Tenant__r.City__c,Tenant__r.Id,Tenant__r.IsActive__c,Tenant__r.Name,Tenant__r.Organization__r.Name,Tenant__r.Organization__c,Tenant__r.Primary_Contact_Email__c,Tenant__r.Primary_Contact_Mobile__c,Tenant__r.Primary_Contact_Name__c,Tenant__r.Tenant_Banner_Url__c,Tenant__r.Tenant_Footer_Message__c,Tenant__r.Tenant_Logo_Url__c,Tenant__r.Tenant_News_Message__c,Tenant__r.Tenant_Short_Name__c,Tenant__r.Tenant_Site_Name__c,Tenant__r.Tenant_Swift_Code__c,Tenant__r.Tenant_Type__c,Tenant__r.Tenant_User_Count__c,Tenant__r.Tenant_vision__c,Tenant__r.Tenant_Website__c,Tenant__r.User_Activation_Email__c FROM Tenant_Mapping__c';
            system.debug('&&'+UserInfo.getUserId());
            system.debug('&&'+System.Label.Admin_ID);
            
            if(TID  == System.Label.TradeAix_Tenant_Id)
            {
                retValue.Adminchk='A';
                query = query + whereCondition +' AND Tenant__r.Tenant_Bank_Type__c=\'Issuing Bank\' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
           
            }else{
               query = query + whereCondition +' AND Tenant_Approved__c=\''+TID+'\' AND Tenant__r.Tenant_Bank_Type__c=\'Issuing Bank\' ORDER BY ' + sortbyField + ' ' + sortDirection + ' LIMIT ' + retValue.list_size + ' offset ' + counter;
            }
            system.debug('&&'+query);
            
            Set<Id> TMSet = new Set<Id>();
            List<Tenant_Mapping__c> TMList = new List<Tenant_Mapping__c>();
            for (Tenant_Mapping__c s : Database.query(query)) {
                system.debug('&sss&'+s.Tenant__c);
                if (TMSet.add(s.Tenant__c)) {
                    TMList.add(s);
                }
                 
            }
              system.debug('&&'+TMSet.size());
            
            retValue.TMresults = TMList;
            
            retValue.total_size = TMSet.size();
            retValue.counter = counter; 
            retValue.sortbyField = sortbyField; 
            retValue.sortDirection = sortDirection; 
            retValue.total_page = getTotalPages(retValue.list_size, retValue.total_size);
            retValue.selectedItem = (sessionselectedchk == '') ? 'Active': sessionselectedchk;
        } catch(Exception ex){
            System.debug('LightBankList - ShowTable() : ' + ex.getMessage());
            
        }
        
        return retValue;    
    }
    @AuraEnabled
    public static String EditBank(String  ccid) {
        String response = '';
       
        return response; 
    }
    
    @AuraEnabled
    public static LightBankListV getBankList(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID){           
        LightBankListV retValue = new LightBankListV(); 
        try{  
            if((String)Cache.Session.get('sortbyFieldSOL') != null)  
                sortbyField = Helper.getSessionValue('sortbyFieldSOL');  
            
            if((String)Cache.Session.get('sortDirectionSOL') != null)  
                sortDirection = Helper.getSessionValue('sortDirectionSOL'); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID);  
        } catch(Exception ex){
            System.debug('LightBankList - ShowTable() : ' + ex.getMessage());
            
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightBankListV getBeginning(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV Views(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        Helper.setSessionValue('sessionselected', selected);
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV getPrevious(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) {
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV getNext(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) {  
        return ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
    }
    
    @AuraEnabled
    public static LightBankListV getEnd(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        return ShowTable(counter, sortbyField, sortDirection, selected,Filtervalue,TID); 
    } 
    
    private static Integer getTotalPages(Integer list_size, Integer total_size) { 
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        }
        else {
            return (total_size/list_size);
        }
    }  
    
    @AuraEnabled
    public static LightBankListV changelist_size(Integer counter, String sortbyField, String sortDirection, Integer list_size, String selected,String Filtervalue,String TID) { 
        LightBankListV retValue = new LightBankListV(); 
        try{
            size = [SELECT count() from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList'];
            if(size > 0){  
                UI_List_Size__c listSize = new UI_List_Size__c(
                    Id = [SELECT Id from UI_List_Size__c WHERE User__c =: UserInfo.getUserId() AND Name =: 'BankList' AND Grid_Name__c =: 'BankList'][0].Id,
                    List_Size__c = list_size  
                );
                update listSize;
            }
            else {
                UI_List_Size__c listSize = new UI_List_Size__c(
                    List_Size__c = list_size,
                    Name = 'BankList',
                    Grid_Name__c = 'BankList',
                    User__c = UserInfo.getUserId()
                );
                insert listSize;
            }  
            
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID);
            
        } catch(Exception ex){
            
        }
        return retValue;
    }
    
    @AuraEnabled
    public static LightBankListV SortTable(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) { 
        LightBankListV retValue = new LightBankListV(); 
        try{
            if(sortbyField == '') {
                sortbyField = 'LastModifiedDate';
                sortDirection = 'DESC';
            } else {
                if(sortDirection.equals('ASC')){
                    sortDirection = 'DESC';
                } else {
                    sortDirection = 'ASC';
                }
            }
            Helper.setSessionValue('sortbyFieldSOL', sortbyField);
            Helper.setSessionValue('sortDirectionSOL', sortDirection); 
            retValue = ShowTable(counter, sortbyField, sortDirection, selected, Filtervalue,TID); 
        } catch (Exception ex){ 
           
        }
        return retValue;
    } 
    
    @AuraEnabled
    public static LightBankListV changePage(Integer counter, String sortbyField, String sortDirection, String selected,String Filtervalue,String TID) {
        return ShowTable(counter, sortbyField, sortDirection, selected,Filtervalue,TID); 
    }
    
   
   
}