public class LightDocuSignEnvelopeInfoV {
    @AuraEnabled
    public String envelopeId = '';
    @AuraEnabled
    public String uri = ''; 
    @AuraEnabled 
    public String statusDateTime = '';
    @AuraEnabled
    public String status = '';  
}