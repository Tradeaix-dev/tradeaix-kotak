public without sharing class CPInvitationEmalTemp {
public Id tenantuserId {get;set;} 
    
    public class UserDetails { 
        public String userId {get;set;}
        public String username {get;set;}
        public String sendername {get;set;}
        public String sitename {get;set;}  
        public String Thanksmsg{get;set;}
        public String logo{get;set;}
        public String logotitle{get;set;} 
        public String Transref{get;set;} 
        public String OTP{get;set;} 
        public String LB{get;set;}
        public String InvitedBy{get;set;}
        public Boolean InvitedbyColleague {get;set;}
        public Boolean Invitedfortaskassignee {get;set;}
    }
    
    public UserDetails email {get;set;}  
    public UserDetails getUsers()
    {
        email = new UserDetails();
        try{
            
            Limited_Bidding__c UM = [SELECT Id,Transaction_Reference__c,Tenant_User__c from Limited_Bidding__c WHERE Id=:tenantuserId];
            system.debug('##'+UM);
            User_Management__c UM1 = [SELECT Id,First_Name__c,Invited_By__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c, Task_Assignee__c FROM User_Management__c WHERE id=:UM.Tenant_User__c];
            system.debug('##'+UM1);
            User_Management__c UM2 = [SELECT Id,First_Name__c,Last_Name__c,Name,CreatedBy__c,User_Name__c,Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Logo_Url__c, Tenant__r.Tenant_Footer_Message__c, Task_Assignee__c FROM User_Management__c WHERE id=:UM1.CreatedBy__c];
            
            email.Transref = UM.Transaction_Reference__c;
            email.LB= UM.Id;
            email.username = UM1.First_Name__c;
            email.sitename = UM2.Tenant__r.Tenant_Site_Name__c;
            email.logo = UM2.Tenant__r.Tenant_Logo_Url__c;
            email.sendername =UM2.First_Name__c+' '+UM2.Last_Name__c;
            email.Thanksmsg= UM2.Tenant__r.Tenant_Footer_Message__c;
            email.InvitedBy = UM1.Invited_By__c;
            if(UM1.Invited_By__c !=null)
            {
            email.InvitedbyColleague =true;
            }else{
            email.InvitedbyColleague =false;
            }
            if(UM1.Task_Assignee__c !=null)
            {
                email.Invitedfortaskassignee = true;
            }else{
                email.Invitedfortaskassignee = false;
            }
            if(UM2.Tenant__r.Tenant_Logo_Url__c == null){
                email.logo = system.label.TradeAix_Logo;
            }
            email.userId = UM1.Id;
        }catch(exception ex){
            system.debug('//****// '+ex.getMessage());
            email.sitename = ex.getMessage()+':::'+string.valueOf(ex.getLineNumber()+'::'+tenantuserId);
        }
        return email;
    }
}