public class UserActiveNotificationEmailTemp {
	public Id userId {get;set;} 
    public class userDetails {
        public String firstname{get;set;}
        public string username {get;set;}
        public String logo{get;set;}
        public String urlname{get;set;}
        public String tenantname {get;set;}
        public String userid {get;set;}
        
    }
    public userDetails email {get;set;}  
    public userDetails getUsers()
    {
        email = New userDetails();
        User_Management__c UM = [SELECT Id, Name, First_Name__c, Last_Name__c, User_Name__c, User_Email__c, Tenant__r.Tenant_Site_Name__c from User_Management__c WHERE ID =: userId];
        email.firstname = UM.First_Name__c;
        email.username = UM.User_Name__c;
        email.logo =system.label.TradeAix_Logo;
        email.tenantname = UM.Tenant__r.Tenant_Site_Name__c;
        email.userid = UM.Id;
        try{
        }catch(exception e){
            system.debug('//****// '+e.getMessage());
        }
        return email;
    }
}