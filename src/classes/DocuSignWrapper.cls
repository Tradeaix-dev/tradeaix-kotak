public with sharing class DocuSignWrapper {
    
    private static string DocuSignUrl {
        get{ 
            return Helper.getDocuSignSettingValue('DocuSignUrl');
        }
    }
    
    private static string DocuSignUserName {
        get{
            return Helper.getDocuSignSettingValue('DocuSignUserName');
        }
    }
    
    private static string DocuSignPassword {
        get{
            return Helper.getDocuSignSettingValue('DocuSignPassword');
        }
    }
    
    private static string DocuSignIntegratorKey {
        get{
            return Helper.getDocuSignSettingValue('DocuSignIntegratorKey');
        }
    }
    
    private static string DocuSignTransactionCompleteDocId {
        get{
            return Helper.getDocuSignSettingValue('DocuSignTransactionCompleteDocId');
        }
    }
    
    private static string DocuSignEmailSubject {
        get{
            return Helper.getDocuSignSettingValue('DocuSignEmailSubject');
        }
    }
    
    private static string DocuSignEmailBody {
        get{
            return Helper.getDocuSignSettingValue('DocuSignEmailBody');
        }
    }
    
    
    private static string ReplyEmailName {
        get{
            return Helper.getDocuSignSettingValue('ReplyEmailName');
        }
    }
    
    private static string ReplyEmailAddress {
        get{
            return Helper.getDocuSignSettingValue('ReplyEmailAddress');
        }
    }
    
    
    private static string FileType {
        get{
            return Helper.getDocuSignSettingValue('FileType');
        }
    }
    
    private static User GetCurrentUser() {
        return [SELECT Id, Name, Email, Title FROM User WHERE Id =: UserInfo.getUserId()];
    }
    
    private static HttpRequest CreateDocuSignReqest() {
        HttpRequest request = new HttpRequest(); 
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Accept', 'application/json');
        request.setHeader('X-DocuSign-Authentication', '{"Username":"' + DocuSignUserName 
                          + '","Password":"' + DocuSignPassword 
                          + '","IntegratorKey": "' + DocuSignIntegratorKey + '"}');
        return request;
    }
    
    private static HttpRequest CreateEnvelopeReqest(string baseUrl) {
        HttpRequest request = CreateDocuSignReqest(); 
        request.setMethod('POST');
        request.setEndpoint(baseUrl + '/envelopes'); 
        return request;
    }
    
    private static HttpRequest CreateLoginReqest(string baseUrl) {
        HttpRequest request = CreateDocuSignReqest(); 
        request.setMethod('GET');
        request.setEndpoint(baseUrl);
        return request;
    }
    
    private static LightDocuSignLoginInfoV LoginInformation() { 
        HttpRequest request = CreateLoginReqest(DocuSignUrl);            
        HTTPResponse response = new Http().send(request);
        return (LightDocuSignLoginInfoV)System.JSON.deserialize(response.getBody(), LightDocuSignLoginInfoV.class);
    }
    
    @Testvisible
    private static Attachment GetDocumentData(string offerId) {
        
        /*StaticResource reuslt = [
            SELECT 
                Id, 
                Body, 
                Name, 
                Description 
            FROM StaticResource 
            WHERE Id =: DocuSignTransactionCompleteDocId
        ];
		reuslt.Name = 'TransactionCompletionDocument';
        reuslt.Description = FileType;*/
        Attachment reuslt =[SELECT 
                Id, 
                Body, 
                Name, 
                Description 
            FROM Attachment 
            WHERE ParentId =:offerId LIMIT 1
        ]; 
        reuslt.Name = 'TransactionCompletionDocument';
        reuslt.Description = FileType;
        return reuslt;
    }
    
    private static DocuSignEnvelopeDocument__c UpsertEnvelopeDocument(string EnvelopeId, Attachment docData, string docuVersion){
        DocuSignEnvelopeDocument__c envelopeDocument = new DocuSignEnvelopeDocument__c( 
            Name = docData.Name, 
            EnvelopeId__c = EnvelopeId,
            Type__c = docData.Description,
            Version__c = docuVersion
        );
        upsert envelopeDocument;
        return envelopeDocument;
    }
    
    private static DocuSignEnvelope__c UpsertEnvelope(string envelopeId, string approverEmail, string senderEmail, 
                                                      string approverName, string approverTitle, string signupUserId)
    {
        system.debug('##envelopeId##'+envelopeId);
        system.debug('##approverEmail##'+approverEmail);
        system.debug('##senderEmail##'+senderEmail);
        system.debug('##approverName##'+approverName);
        system.debug('##approverTitle##'+approverTitle);
        system.debug('##signupUserId##'+signupUserId);
        DocuSignEnvelope__c envelope = new DocuSignEnvelope__c( 
            Name = envelopeId,
            Approver_Email__c = approverEmail,
            Sender_Email__c = senderEmail,
            Approver_Name__c = approverName,
            Approver_Title__c = approverTitle            
        );
        system.debug('##Before Upsert##');
        upsert envelope;
        system.debug('##After Upsert##');
        return envelope;
    }
    
    @Testvisible
    private static string CreateRequestBody(string approver1Email, string approver1Name, 
                                            string approver2Email, string approver2Name,
                                            string transactionName, string transactionCompletedDate, 
                                            string partyBankName, string counterPartyBankName,
                                            Attachment docData) 
    {
        
        string emailBlurb = DocuSignEmailBody;
        List<String> fillers = new String[]{approver1Name};
            emailBlurb = String.format(emailBlurb, fillers);
        /*
        return '{'
            + '\'recipients\': {' 
            + '\'signers\': ['
            + '{'
            + '\'email\': \''+ approver1Email +'\','
            + '\'name\': \''+ approver1Name +'\','
            + '\'recipientId\': \'1\','
            + '\'routingOrder\': \'1\','
            + '\'tabs\': {'
            + '\'signHereTabs\': ['
            + '{'
            + '\'xPosition\': \'71\','
            + '\'yPosition\': \'280\','
            + '\'documentId\': \'1\','
            + '\'pageNumber\': \'4\''
            + '}'
            + '],'
            + '\'textTabs\': ['
            + '{'
            + '\'tabId\': \'1\','
            + '\'name\': \'Transaction Name\','
            + '\'tabLabel\': \'Transaction Name\','
            + '\'value\': \''+ transactionName +'\','
            + '\'locked\': \'false\','
            + '\'xPosition\': \'245\','
            + '\'yPosition\': \'224\','
            + '\'documentId\': \'1\','
            + '\'pageNumber\': \'1\''
            + '},'
            + '{'
            + '\'tabId\': \'2\','
            + '\'name\': \'Transaction Completed Date\','
            + '\'tabLabel\': \'Transaction Completed Date\','
            + '\'value\': \''+ transactionCompletedDate +'\','
            + '\'locked\': \'false\','
            + '\'xPosition\': \'245\','
            + '\'yPosition\': \'245\','
            + '\'documentId\': \'1\','
            + '\'pageNumber\': \'1\''
            + '},'
            + '{'
            + '\'tabId\': \'3\','
            + '\'name\': \'Party Bank\','
            + '\'tabLabel\': \'Party bank\','
            + '\'value\': \''+ partyBankName +'\','
            + '\'locked\': \'false\','
            + '\'xPosition\': \'71\','
            + '\'yPosition\': \'470\','
            + '\'documentId\': \'1\','
            + '\'pageNumber\': \'1\''
            + '},' 
            + '{'
            + '\'tabId\': \'4\','
            + '\'name\': \'Counter Party Bank\','
            + '\'tabLabel\': \'Counter Party Bank\','
            + '\'value\': \''+ counterPartyBankName +'\','
            + '\'locked\': \'false\','
            + '\'xPosition\': \'316\','
            + '\'yPosition\': \'470\','
            + '\'documentId\': \'1\','
            + '\'pageNumber\': \'1\''
            + '}'
            + ']'
            + '}'
            + '}'
            + ']'
            + '},'
            + '\'emailSubject\': \''+ DocuSignEmailSubject +'\','
            + '\'emailBlurb\': \''+ emailBlurb +'\','
            + '\'documents\': ['
            + '{'
            + '\'documentId\': \'1\','
            + '\'name\': \''+ docData.Name + '.' + docData.Description +'\','                      
            + '\'documentBase64\': \''+ EncodingUtil.base64Encode(docData.Body) +'\'' 
            + '}'                           
            + '],'
            + '\'status\': \'sent\','
            + '\'emailSettings\': {'
            + '\'replyEmailAddressOverride\': \''+ ReplyEmailAddress +'\','
            + '\'replyEmailNameOverride\': \''+ ReplyEmailName +'\''
            + '},'
            + '\'notification\': {'
            + '\'useAccountDefaults\': \'true\''
            + '}'
            + '}';   
		*/
        return '{'
            + '\'recipients\': {' 
            + '\'signers\': ['
            + '{'
            + '\'email\': \''+ approver1Email +'\','
            + '\'name\': \''+ approver1Name +'\','
            + '\'recipientId\': \'1\','
            + '\'routingOrder\': \'1\','
            + '\'tabs\': {'
            + '\'signHereTabs\': ['
            + '{'
            + '\'xPosition\': \'87\','
            + '\'yPosition\': \'346\','
            + '\'documentId\': \'1\','
            + '\'pageNumber\': \'2\''
            + '}'
            + ']'
            + '}'
            + '},'
            + '{'
            + '\'email\': \''+ approver2Email +'\','
            + '\'name\': \''+ approver2Name +'\','
            + '\'recipientId\': \'2\','
            + '\'routingOrder\': \'2\','
            + '\'tabs\': {'
            + '\'signHereTabs\': ['
            + '{'
            + '\'xPosition\': \'314\','
            + '\'yPosition\': \'349\','
            + '\'documentId\': \'1\','
            + '\'pageNumber\': \'2\''
            + '}'
            + ']'
            + '}'
            + '}'
            + ']'
            + '},'
            + '\'emailSubject\': \''+ DocuSignEmailSubject +'\','
            + '\'emailBlurb\': \''+ emailBlurb +'\','
            + '\'documents\': ['
            + '{'
            + '\'documentId\': \'1\','
            + '\'name\': \''+ docData.Name + '.' + docData.Description +'\','                      
            + '\'documentBase64\': \''+ EncodingUtil.base64Encode(docData.Body) +'\'' 
            + '}'                           
            + '],'
            + '\'status\': \'sent\','
            + '\'emailSettings\': {'
            + '\'replyEmailAddressOverride\': \''+ ReplyEmailAddress +'\','
            + '\'replyEmailNameOverride\': \''+ ReplyEmailName +'\''
            + '},'
            + '\'notification\': {'
            + '\'useAccountDefaults\': \'true\''
            + '}'
            + '}';   
		
    }
    
    public static HttpRequest CreateGetEnvelopeDocumentReqest(string baseUrl, string envelopeId, string documentId) {
        HttpRequest request = CreateDocuSignReqest(); 
        request.setMethod('GET');
        
        string url = '/envelopes/{0}/documents/{1}';
        List<String> fillers = new String[]{envelopeId, documentId};
        url = String.format(url, fillers);
        
        request.setEndpoint(baseUrl + url); 
        return request;
    }    
    
    public static Blob GetSignedDocument(string envelopeId, string documentId) {
        LightDocuSignLoginInfoV loginInfo = LoginInformation(); 
        HTTPResponse response;
        try{
            
            HttpRequest request = CreateGetEnvelopeDocumentReqest(loginInfo.loginAccounts[0].baseUrl, envelopeId, documentId); 
            response = new Http().send(request);
        }
        catch (Exception ex){
          /*  Activity_Log__c log = new Activity_Log__c( 
                Name = 'GetSignedDocument',
                LogMessage__c = 'Exception : ' + ex.getMessage()
            );
            insert log;*/
        }  
        return response.getBodyAsBlob();
    }
    
    //@AuraEnabled
    @future (callout=true)
    public static void CreateEnvelope(
        string approverEmail, string approverName, string approverTitle,
        string approver2Email, string approver2Name, string approver2Title,
        string transactionName, string transactionCompletedDate, 
        string partyName,string counterPartyName,string offerId
    ) {   
        try {
            system.debug('##Inside Try##');
            system.debug('##approverEmail##'+approverEmail);
            system.debug('##approverName##'+approverName);
            system.debug('##approverTitle##'+approverTitle);
            system.debug('##approver2Email##'+approver2Email);
            system.debug('##approver2Name##'+approver2Name);
            system.debug('##approver2Title##'+approver2Title);
            system.debug('##transactionName##'+transactionName);
            system.debug('##transactionCompletedDate##'+transactionCompletedDate);
            system.debug('##partyName##'+partyName);
            system.debug('##counterPartyName##'+counterPartyName);
            
            Attachment docData = GetDocumentData(offerId); 
            
            User user = GetCurrentUser();  
            
            LightDocuSignLoginInfoV loginInfo = LoginInformation();  
            system.debug('##loginInfo##'+loginInfo);
            HttpRequest request = CreateEnvelopeReqest(loginInfo.loginAccounts[0].baseUrl); 
             system.debug('##request##'+request);
            request.setBody(
                CreateRequestBody(
                    approverEmail, approverName, 
                    approver2Email, approver2Name,
                    transactionName, transactionCompletedDate, 
                    partyName, counterPartyName, 
                    docData
                )
            );
             system.debug('##request##'+request);
            HTTPResponse response = new Http().send(request); 
            system.debug('##response##'+response);
            LightDocuSignEnvelopeInfoV envelopesInfo = (LightDocuSignEnvelopeInfoV)System.JSON.deserialize(
                response.getBody(), 
                LightDocuSignEnvelopeInfoV.class
            );
             system.debug('##envelopesInfo##'+envelopesInfo);
            system.debug('##UserEmail##'+user.Email);
            DocuSignEnvelope__c envelope = UpsertEnvelope(
                envelopesInfo.envelopeId, approverEmail, user.Email,  
                approverName, approverTitle, ''
            );
            system.debug('##envelope##'+envelope.Id);
            system.debug('##docData##'+docData);
            UpsertEnvelopeDocument(envelope.Id, docData, 'V1.0');

        }
        catch(Exception ex)
        { 
            //Activity_Log__c log = new Activity_Log__c( 
             //   Name = 'Step 3 ',
            //    LogMessage__c = 'Exception : ' + ex.getMessage()
            //);
           // insert log;
        }
    }
    
    public static void DummyClasses()
    {
        Double value = 0;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
        value = value + 1;
    } 

}