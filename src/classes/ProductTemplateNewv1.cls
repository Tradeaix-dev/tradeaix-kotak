public class ProductTemplateNewv1 {
    @AuraEnabled 
    public String ProductType {get; set;}
     @AuraEnabled 
    public ProductTemplate__c PT{get; set;}
    @AuraEnabled
    public List<Product_Template_Object__c> strList { get;set; }
}