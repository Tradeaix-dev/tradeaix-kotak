public class ProductTemplatevNEW {
    @AuraEnabled 
    public String ProductType {get; set;}
    @AuraEnabled 
    public String CreatedByName {get; set;}
    @AuraEnabled 
    public String LastModifiedByName {get; set;}
    @AuraEnabled 
    public String TemplateName {get; set;}
    @AuraEnabled 
    public String Description {get; set;}
    @AuraEnabled 
    public Boolean IsPrimary = false; 
    @AuraEnabled  
    public Boolean IsSecondary = false;
    @AuraEnabled  
    public Boolean icclonechk = false;
    @AuraEnabled 
    public List<TemplateField> SourceFields {get;set;}
    @AuraEnabled 
    public List<String> SelectedFields {get;set;}
    @AuraEnabled 
    public String ApproveLevel {get; set;}
     @AuraEnabled
    public ProductTemplateV ProductTemplateVExtended = new ProductTemplateV();
    @AuraEnabled 
    public ProductTemplate__c ProductTemplateaa {get; set;}
    @AuraEnabled
    public user ProdTempCreatedUser {get; set;}
    @AuraEnabled
    public user ProdTempLstModifiedUser {get; set;}
}