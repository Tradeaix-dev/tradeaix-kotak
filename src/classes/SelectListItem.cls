public class SelectListItem {
    
    @AuraEnabled 
    public String label {get; set;}
    @AuraEnabled 
    public String value {get; set;}
    @AuraEnabled 
    public Boolean selected {get; set;}
    
    public SelectListItem(String value, String label, Boolean selected) {
        this(value, label);
        this.selected = selected;
    }
    
    public SelectListItem(String value, String label) {
        this.label = label;
        this.value = value;
        this.selected = false;
    }
}