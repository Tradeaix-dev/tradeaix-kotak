public class TemplateAttributesV {
    @AuraEnabled 
    public String Name {get; set;}
    @AuraEnabled 
    public String AttributeType {get; set;}
    @AuraEnabled 
    public Integer AttributeSize {get; set;}
    @AuraEnabled 
    public Boolean IsActive = true;
    @AuraEnabled 
    public Boolean IsPrimary {get; set;}
    @AuraEnabled 
    public Boolean IsSecondary {get; set;} 
}