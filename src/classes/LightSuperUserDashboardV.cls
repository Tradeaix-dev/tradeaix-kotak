public class LightSuperUserDashboardV {
    @AuraEnabled // Super User Dashbpard Header Data
    public LightSuperUserDashboardHeaderV Header = new LightSuperUserDashboardHeaderV();
    @AuraEnabled
    public integer createdcount =0;
    @AuraEnabled
    public integer Publishedcount =0; 
    @AuraEnabled
    public integer RFICount =0;
    @AuraEnabled
    public integer Biddingcount =0;
    @AuraEnabled
    public integer Completedcount =0;
    
    
}