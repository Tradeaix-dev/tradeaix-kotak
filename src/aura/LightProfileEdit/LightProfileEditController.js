({ 
    selectTab : function(component, event, helper) { 
        /* General utility */
        var selected = component.get("v.key")
        component.find("tabs").set("v.selectedTabId",selected);
    },
	getUserList : function(component, event, helper) {
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        
        var userId = getUrlParameter('userid');
        component.set("v.userId",userId);
        component.set("v.flag",getUrlParameter('c'));
        var Action = component.get("c.getUserprofile");
        var sessionname = localStorage.getItem("UserSession");
        Action.setParams({
            "username" : userId
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.TenantUser",results.Userlists);  
                component.set("v.users", results);
                 if(results.usernotificationLists.length > 0){  
                    for (var i = 0; i < results.usernotificationLists.length; i++) { 
                        if(results.usernotificationLists[i].Name == 'Create Transaction'){
                            component.set("v.CreateTransaction",results.usernotificationLists[i].Notification__c);
                            component.set("v.CreateTransactionEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Validate Transaction'){
                            component.set("v.ValidateTransaction",results.usernotificationLists[i].Notification__c);
                            component.set("v.ValidateTransactionEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Approve Transaction'){
                            component.set("v.ApproveTransaction",results.usernotificationLists[i].Notification__c);
                            component.set("v.ApproveTransactionEmail",results.usernotificationLists[i].Email__c);
                        }                       
                        if(results.usernotificationLists[i].Name == 'Request for Information'){
                            component.set("v.RequestforInformation",results.usernotificationLists[i].Notification__c);
                            component.set("v.RequestforInformationEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Request For Quote'){
                            component.set("v.RequestforQuote",results.usernotificationLists[i].Notification__c);
                            component.set("v.RequestforQuoteEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Submission'){
                            component.set("v.BidSubmission",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidSubmissionEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Accept'){
                            component.set("v.BidAccept",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidAcceptEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Reject'){
                            component.set("v.BidReject",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidRejectEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Closed'){
                            component.set("v.BidClosed",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidClosedEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Inactive'){
                            component.set("v.BidInactivate",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidInactivateEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Execute Agreement'){
                            component.set("v.ExecuteAgreement",results.usernotificationLists[i].Notification__c);
                            component.set("v.ExecuteAgreementEmail",results.usernotificationLists[i].Email__c);
                        }                       
                        
                    }
                }
            }
        });
        $A.enqueueAction(Action);
    },
    
    saveUserprofile : function(component, event, helper) {
        debugger;  
        var Action1 = component.get("c.saveprofile");
        var sessionname = localStorage.getItem("UserSession");
        Action1.setParams({
            "username" : sessionname ,
            "tenant" : component.get("v.TenantUser")
        });
        Action1.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                var site = $A.get("{!$Label.c.Org_URL}");
                window.location = '/'+site+'/s/dashboard';
            }
        });
        $A.enqueueAction(Action1);
        
    },     
    editUser: function(component, event, helper) { 
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/user-create?userid='+component.get("v.userId");
    },
    updateMynotifications: function(component, event, helper) {
        debugger;
        var notificationIds=new Array();
        var ctN = component.find("chkboxcreateTransaction").get("v.value");
        var ctE = component.find("chkboxcreateTransactionemail").get("v.value");
        notificationIds.push("Create Transaction:"+ctN+":"+ctE);
        var vtN = component.find("chkboxvalidateTransaction").get("v.value");
        var vtE = component.find("chkboxvalidateTransactionemail").get("v.value");
        notificationIds.push("Validate Transaction:"+vtN+":"+vtE);
        var atN = component.find("chkboxapproveTransaction").get("v.value");
        var atE = component.find("chkboxapproveTransactionemail").get("v.value");
        notificationIds.push("Approve Transaction:"+atN+":"+atE);
        var rtN = component.find("chkboxrequestTransaction").get("v.value");
        var rtE = component.find("chkboxrequestTransactionemail").get("v.value");
        notificationIds.push("Request for Information:"+rtN+":"+rtE);
        var rqN = component.find("chkboxrequestQuote").get("v.value");
        var rqE = component.find("chkboxrequestQuoteemail").get("v.value");
        notificationIds.push("Request For Quote:"+rqN+":"+rqE);
        var bsN = component.find("chkboxbidsubmission").get("v.value");
        var bsE = component.find("chkboxbidsubmissionemail").get("v.value");
        notificationIds.push("Bid Submission:"+bsN+":"+bsE);
        var baN = component.find("chkboxbidAccept").get("v.value");
        var baE = component.find("chkboxbidAcceptemail").get("v.value");
        notificationIds.push("Bid Accept:"+baN+":"+baE);
        var brN = component.find("chkboxbidReject").get("v.value");
        var brE = component.find("chkboxbidRejectemail").get("v.value");
        notificationIds.push("Bid Reject:"+brN+":"+brE);
        var bcN = component.find("chkboxbidClosed").get("v.value");
        var bcE = component.find("chkboxbidClosedemail").get("v.value");
        notificationIds.push("Bid Closed:"+bcN+":"+bcE);
        var biN = component.find("chkboxbidInactive").get("v.value");
        var biE = component.find("chkboxbidInactiveemail").get("v.value");
        notificationIds.push("Bid Inactive:"+biN+":"+biE);
        var eaN = component.find("chkboxexeAgreement").get("v.value");
        var eaE = component.find("chkboxexeAgreementemail").get("v.value");
        notificationIds.push("Execute Agreement:"+eaN+":"+eaE);
        var idListJSON=JSON.stringify(notificationIds);
        // alert(idListJSON);
        var action = component.get("c.changeMynotifications");
        var sessionname = localStorage.getItem("UserSession");
        action.setParams({
            "username" : component.get("v.userId"),
            "listnotify":idListJSON
        });
        action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){               
                
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Notification Updated Successfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                //$A.get('e.force:refreshView').fire();
                component.set("v.key","four");
                           
            }
        });
        $A.enqueueAction(action);
    },
     backtoAdmin : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/administration';
    },
    backtouser : function(component, event, helper) {
        debugger;
        if(component.get("v.flag")=='u'){
        var site = $A.get("{!$Label.c.Org_URL}");
            window.location = '/'+site+'/s/adminusers';
        }else{
             var site = $A.get("{!$Label.c.Org_URL}");
            window.location = '/'+site+'/s/counterparty-bank';
        }
    }
})