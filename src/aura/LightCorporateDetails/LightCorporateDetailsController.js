({    
    selectTab : function(component, event, helper) { 
        /* General utility */
        var selected = component.get("v.key");
        component.find("tabs").set("v.selectedTabId",selected);
    },
    getBank: function(component) {
        var ACP = $A.get("{!$Label.c.ACP_ProfileID}");
         if(localStorage.getItem("LoggeduserProfile") == ACP){
            component.set("v.IsTradeAixAdmin",true);
         }else{
             component.set("v.IsTradeAixAdmin",false);
         }
        debugger;
        var action = component.get('c.getBankDetails');         
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        component.set("v.FID",getUrlParameter('flag'));
        var bankId = getUrlParameter('bankId');
        //alert(localStorage.getItem("LoggeduserTenantID"));
        action.setParams({
            "bankId" : bankId,
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID")
        });
        action.setCallback(this, function(actionResult) { 
            
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Result", result);
                component.set("v.Counterparty",result.TM);
                component.set("v.Tenants",result.TList);
                component.set("v.Activity_Log",result.Activity_Log);
                component.set("v.TType", result.result.Tenant_Type__c);
                component.set("v.TbankType", result.result.Tenant_Bank_Type__c);
                component.set("v.Attachmentid", result.Attachmentid);
                component.set("v.bankId", result.result.Id);
                component.set("v.oldBankName", result.result.CU_Name__c); 
                component.set("v.Users",result.userResults);
                component.set("v.createuser",false);
                component.set("v.userview",true);
                
                var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
       		    if(component.get("v.TType")=='Corporate')
                {document.title =sitename +' : Corporate Detail';
                }else if(component.get("v.TbankType")=='Counterparty Bank')
                { document.title =sitename +' : Counterparty Bank Detail';
                }
                else if(component.get("v.TbankType")=='Party Bank')
                { document.title =sitename +' : Party Bank Detail';
                }
                else if(component.get("v.TbankType")=='Issuing Bank')
                { document.title =sitename +' : Issuing Bank Detail';
                }
                var indetails = {};
                indetails.FirstName = '';
                indetails.LastName = '';
                indetails.JobTitle = '';
                indetails.re_email = '';
                indetails.Email = '';
                indetails.Zipcode = '';
                indetails.Phone_No = '';
                indetails.Username ='';
                component.set("v.inputter", indetails);
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.Result"); 
                component.set("v.Result", emptyTask);
            }
            
            debugger;
            var menu = document.getElementById('Corporates');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
            
        }); 
        $A.enqueueAction(action);
    },counterPopupCancel1 : function(component, event) {
        debugger;
        component.set("v.counterPopup", false);
    }, counterPopupCancel : function(component, event) {        
        component.set("v.counterPopup", false);
        $A.get('e.force:refreshView').fire();
        component.set("v.key","two");
    },
     CPConfirmpopup : function(component, event) {       
        component.set("v.counterPopup", false);
        component.set("v.CPConfirm", true);
        
    }, 
    selectCounterParties: function(component, event) {
        debugger;
        var checkbox = event.getSource();
        var checked = checkbox.get("v.value");
        if(checked) {
            component.find("btnSelect").set("v.disabled", false);
        }
        else{
            component.find("btnSelect").set("v.disabled", true);
        }
        var creditUnionId = checkbox.get("v.text");
        debugger;
        
        var action = component.get('c.addRemoveCounterParty');  
        action.setParams({
            "creditUnionId" : creditUnionId,
            "forAdd" : checked,
            "TId" : component.get("v.bankId"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
        
        
    },
      CPrevokepopup: function(component, event) {
        debugger;
        var action = component.get('c.addRemoveCounterParty');  
        action.setParams({
            "creditUnionId" : event.target.id,
            "forAdd" : false,
            "TId" : component.get("v.bankId"),
            "LoggedUserId" : localStorage.getItem('UserSession')
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
        
        
    },
    CPConfirmcan: function(component, event) {
        component.set("v.CPConfirm", false);
    },
     showCounterParties: function(component, event) {
        debugger;
        var action = component.get('c.getApprovedCreditUnions');
        action.setParams({
            "creditUnionId" : component.get("v.bankId")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                component.set("v.ApprovedCounterParties", result);
                component.set("v.counterPopup", true);
            }
        });
        $A.enqueueAction(action);
    },
    editBank: function(component, event, helper) {   
        component.set("v.showEdit", true);     
    },
    Editbutton: function(component, event, helper) {
        debugger;
        var ActionTitle = component.get("c.getuserdetails");
        component.set("v.Edituserid",event.target.id);
        ActionTitle.setParams({ "UserId": event.target.id });
        ActionTitle.setCallback(this, function(actionResultTitle) {
            var statetitle = actionResultTitle.getState(); 
            var resulttitle = actionResultTitle.getReturnValue();
            if(statetitle === "SUCCESS"){
                debugger;
                component.set("v.createuser",true);
                component.set("v.userview",false);                
                component.find("infname").set("v.value",resulttitle.resuser.FirstName);
                component.find("injtitle").set("v.value",resulttitle.resuser.Title);
                component.find("inemail").set("v.value",resulttitle.resuser.Email__c);
                component.find("inlname").set("v.value",resulttitle.resuser.LastName);
                component.find("inphno").set("v.value",resulttitle.resuser.Phone__c);
                component.find("inre_email").set("v.value",resulttitle.resuser.Email__c);
                component.find("uname").set("v.value",resulttitle.resuser.Username);
                component.set("v.oldUserName",resulttitle.resuser.Username);
                
               
            }
        });
        $A.enqueueAction(ActionTitle);
    },
    cancel: function(component, event, helper) {
        component.set("v.showEdit", false);
        component.set("v.validation", false);
    }, 
    editBankDetail: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var bankd = component.get("v.bank");
        var action = component.get("c.CheckBankName");
        action.setParams({ "bankName": bankd.CU_Name__c, "oldBankName": component.get("v.oldBankName") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var bank = response.getReturnValue(); 
            if(bank != '' && bank != undefined) {
                if(bank == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.NameError", 'This Bank Name has already been used. Please enter a new Bank Name.');
                } else {
                    component.set("v.NameError", ''); 
                }
            } else { 
                component.set("v.NameError", ''); 
            }
            if(errorFlag == 'false') {
                component.find("btnEditBank").set("v.disabled", true);                    
                var action1 = component.get("c.EditBank");
                action1.setParams({
                    "creditUnion" : bankd
                }); 
                action1.setCallback(this, function(response1) {
                    var state1 = response1.getState();
                    var bankId = response1.getReturnValue(); 
                    debugger;
                    component.find("btnEditBank").set("v.disabled", false);
                    if(bankId != '' && bankId != undefined) {
                        if(bankId == 'Already Used'){ 
                            component.set("v.validation", true);                     
                            component.set("v.NameError", 'This Bank Name has already been used. Please enter a new Bank Name.');
                        } else {
                            var site = $A.get("{!$Label.c.Org_URL}");
                            window.location = '/'+site+'/s/bank-detail?bankId='+ bankId;
                        }
                    }
                    else{ 
                        component.set("v.validation", false); 
                        component.set("v.showCreate", false); 
                    }
                    
                });
                $A.enqueueAction(action1);
            }
        });
        $A.enqueueAction(action); 
    },
    editBankshow: function(component, event, helper) {   
        component.set("v.showEdit1", true);     
    },
    cancel: function(component, event, helper) {
        component.set("v.showEdit1", false);
        component.set("v.validation", false);
    }, 
    inactiveCheck: function(component, event, helper) {
        if(component.find("chkInactive").get('v.value')==true)
        {
            debugger;
            component.find("btnEditBank").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("btnEditBank").set("v.disabled", "true");
        }
    },
    createuserbtn: function(component, event, helper) {
        debugger;
        var indetails = {};
        indetails.FirstName = '';
        indetails.LastName = '';
        indetails.JobTitle = '';
        indetails.re_email = '';
        indetails.Email = '';
        indetails.Zipcode = '';
        indetails.Phone_No = '';
        indetails.Username ='';
        component.set("v.inputter", indetails);
        component.set("v.createuser",true);
        component.set("v.userview",false);
    },
    cancelcall: function(component, event, helper) {
        debugger;
        component.set("v.createuser",false);
        component.set("v.userview",true);
    },
    savecall: function(component, event, helper) {
        debugger;
        /*********Inputter -START********/
        var Newinputter = component.get("v.inputter");
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        
        if(Newinputter.FirstName == '' || Newinputter.FirstName == undefined || Newinputter.FirstName.trim().length*1 == 0)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.infnameError", 'Input field required'); 
        }
        else
        {
            if(helper.checkSpecialCharecter1(Newinputter.FirstName))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.infnameError", 'Please enter without special characters.'); 
            } 
            else{ 
                component.set("v.infnameError", ''); 
            }
        }
        if(Newinputter.JobTitle == '' || Newinputter.JobTitle == undefined || Newinputter.JobTitle.trim().length*1 == 0)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.injtitleError", 'Input field required'); 
        }
        else
        {
            if(helper.checkSpecialCharecter1(Newinputter.JobTitle))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.injtitleError", 'Please enter without special characters.'); 
            } 
            else{
                component.set("v.injtitleError", ''); 
            }
        }
        if(Newinputter.Email == '' || Newinputter.Email== undefined || Newinputter.Email.trim().length*1 == 0)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.inemailError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(Newinputter.Email))
        {   
            if(!Newinputter.Email.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.inemailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.inemailError", ''); 
            }
            debugger;
            
        }
        if(Newinputter.LastName == '' || Newinputter.LastName == undefined || Newinputter.LastName.trim().length*1 == 0)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.inlnameError", 'Input field required'); 
        }
        else
        {
            if(helper.checkSpecialCharecter1(Newinputter.LastName))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.inlnameError", 'Please enter without special characters.'); 
            } 
            else{
                component.set("v.inlnameError", ''); 
            }
        }
        if(Newinputter.Phone_No == '' || Newinputter.Phone_No == undefined || Newinputter.Phone_No.trim().length*1 == 0)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.inphnoError", 'Input field required'); 
        }
        else if(!$A.util.isEmpty(Newinputter.Phone_No))
        {   
            if(!Newinputter.Phone_No.match(regPhoneNo))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.inphnoError", 'Please enter valid phone number.'); 
            } 
            else{
                component.set("v.inphnoError", ''); 
            }
        }
        
        if(Newinputter.Username == '' || Newinputter.Username == undefined || Newinputter.Username.trim().length*1 == 0)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.unameemailError", 'Input field required'); 
        }
        else
        {
            var action = component.get("c.chkusername");  
            action.setParams({ 
                "Username" : Newinputter.Username,
                "oldUName" : component.get("v.oldUserName")});
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    if(actionResult.getReturnValue()=='Already Used'){
                        errorFlag = 'true';
                        component.set("v.validation", true);
                        component.set("v.unameemailError", 'Duplicate Username.The username already exists in this or another Salesforce organization. Usernames must be unique across all Salesforce organizations. To resolve, use a different username (it does not need to match the users email address).'); 
                    }
                    else{
                        component.set("v.unameemailError", ''); 
                    }
                }
            });
            $A.enqueueAction(action);
        }
        if(Newinputter.re_email == '' || Newinputter.re_email.trim().length*1 == 0)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.inre_emailError", 'Input field required'); 
        }
        else if(Newinputter.Email != Newinputter.re_email)
        {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.inre_emailError", 'Please check the email address'); 
        }
            else if(Newinputter.Email ==Newinputter.re_email)
            {
                component.set("v.inre_emailError", ''); 
            } 
        
        /*********Inputter -END********/
        if(errorFlag == 'false')
        { 
            var action = component.get("c.getloggeduserBrowserTitle1");  
            action.setParams({ 
                "FirstName" : component.get("v.inputter").FirstName,
                "LastName" : component.get("v.inputter").LastName,
                "JobTitle" : component.get("v.inputter").JobTitle,
                "Email" : component.get("v.inputter").Email,
                "Phone_No" : component.get("v.inputter").Phone_No,
                "bid" : component.get("v.bankId"),
                "Editcall" : component.get("v.Edituserid"),
                "uname" : component.get("v.inputter").Username
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    debugger;
                    if(component.get("v.Edituserid") == '' && component.get("v.Edituserid") == null){
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'User Successfully Created.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                    }else{
                       var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'User Successfully Updated.',
                            'type': 'Success'
                        }
                    );
                    showToast.fire(); 
                    }
                    component.set("v.createuser",false);
                    component.set("v.userview",true);
                    $A.get('e.force:refreshView').fire();
                } 
            });
            $A.enqueueAction(action);
        }
    },
    editBankDetail: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var bankd = component.get("v.bank");
        
        if(errorFlag == 'false') {
            component.find("btnEditBank").set("v.disabled", true);                    
            var action1 = component.get("c.EditBank1");
            action1.setParams({
                "creditUnion" : bankd
            }); 
            action1.setCallback(this, function(response1) {
                var state1 = response1.getState();
                var bankId = response1.getReturnValue(); 
                debugger;
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Party Bank has been activated.',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                component.set("v.showEdit1", false);
                $A.get('e.force:refreshView').fire();
            });
            $A.enqueueAction(action1);
        }
        
    },
    RejectCPRequestpop: function(component, event, helper) {   
        component.set("v.showRCPRequest", true);     
    },
    cancelCPRequest: function(component, event, helper) {
        component.set("v.showRCPRequest", false);
        component.set("v.validation", false);
    }, 
    RejectCHK: function(component, event, helper) {
        if(component.find("chkreject").get('v.value')==true)
        {
            debugger;
            component.find("btnEditBank").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("btnEditBank").set("v.disabled", "true");
        }
    },
    RejectCP: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var bankd = component.get("v.bank");
        
        if(errorFlag == 'false') {
            component.find("btnEditBank").set("v.disabled", true);                    
            var action1 = component.get("c.RejectCPForAdmin");
            action1.setParams({
                "creditUnion" : bankd,
                "Reason" :  component.find("body5").get("v.value")
            }); 
            action1.setCallback(this, function(response1) {
                var state1 = response1.getState();
                var bankId = response1.getReturnValue(); 
                debugger;
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Counterparty Bank has been Rejected.',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                component.set("v.showRCPRequest", false);
                $A.get('e.force:refreshView').fire();
            });
            $A.enqueueAction(action1);
        }
        
    }
})