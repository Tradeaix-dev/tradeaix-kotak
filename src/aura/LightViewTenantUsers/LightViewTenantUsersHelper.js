({
	getUsers : function(component, event, helper) {
        debugger;
		var tenantUser = component.get("v.TenandId");
        var tId = component.get("v.TransactionId");
        var action = component.get('c.viewTenantUsersList');  
        action.setParams({
            "tenantId" : tenantUser,
            "transactionId" : tId
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                component.set("v.usersview",result);                
                debugger;
                for (var k = 0; k < result.idString.length; k++) {
                    //start                   
                    var getAllId = component.find("checkdoc");        
                    if(! Array.isArray(getAllId)){
                       
                            component.find("checkdoc").set("v.value", true);                
                       
                    }else{   
                        for (var i = 0; i < getAllId.length; i++) {
                            if(component.find("checkdoc")[i].get("v.text") == result.idString[k]){
                                //alert('sss');
                                component.find("checkdoc")[i].set("v.value", true);
                            }                                                        
                        }
                        
                    }  
                }
                //end
            }
        });
        $A.enqueueAction(action);   
	},
    selectAll: function(component, event, helper) {
        debugger;
        //get the header checkbox value  
        var selectedHeaderCheck = event.getSource().get("v.value");       
        var getAllId = component.find("checkdoc");        
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("checkdoc").set("v.value", true);                
            }else{
                component.find("checkdoc").set("v.value", false);               
            }
        }else{           
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("checkdoc")[i].set("v.value", true);                    
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("checkdoc")[i].set("v.value", false);                    
                }
            } 
        }  
        
        var selectedUser = [];   
        var deselectedUser = [];
        if(!Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                selectedUser.push(getAllId.get("v.text"));
            }else{
                deselectedUser.push(checkvalue.get("v.text"));
            }
        }else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    selectedUser.push(getAllId[i].get("v.text"));
                }
            }
        }
        
        if(selectedUser != ''){    
            var action = component.get('c.addCPTenantUsers');  
            action.setParams({
                "tenantusers" : selectedUser,
                "offerId" : component.get("v.TransactionId")                        
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    if(result == 'Success'){
                        
                    }
                }
            });
            $A.enqueueAction(action);  
        }        
        console.log('selectedUser-' + selectedUser);
        
    },
    
    onchangechkbox : function(component, event, helper){
        debugger;
        var selectedUser = [];
        var deselectedUser = [];
        var TransId = component.get("v.TransactionId");
        var checkvalue = component.find("checkdoc");
        
        if(!Array.isArray(checkvalue)){
            if (checkvalue.get("v.value") == true) {
                selectedUser.push(checkvalue.get("v.text"));
            }else{
                deselectedUser.push(checkvalue.get("v.text"));
            }
        }else{
            for (var i = 0; i < checkvalue.length; i++) {
                if (checkvalue[i].get("v.value") == true) {
                    selectedUser.push(checkvalue[i].get("v.text"));
                }else{
                    deselectedUser.push(checkvalue[i].get("v.text"));
                }
            }
        }
        if(selectedUser != ''){  
            //alert(selectedUser);
            var action = component.get('c.addremovesingleCPTenantUser');  
            action.setParams({
                "tenantusers" : selectedUser,
                "offerId" : component.get("v.TransactionId")                        
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    if(result == 'Success'){
                        
                    }
                }
            });
            $A.enqueueAction(action);  
        }
        if(deselectedUser !=''){
            debugger;
            //alert(deselectedUser);
            var action = component.get('c.removesingleCPTenantUser');  
            action.setParams({
                "tenantdeselectusers" : deselectedUser,
                "offerId" : component.get("v.TransactionId"),
                "LoggedUserId" : localStorage.getItem("IDTenant")
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    if(result == 'Success'){
                        
                    }
                }
            });
            $A.enqueueAction(action);
        }
        console.log('selectedUser-' + selectedUser);
    },
    
})