({
    getTemplateAttributes: function(component, event, helper) { 
        debugger;
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Attribute';
        if(localStorage.getItem('LoggeduserProfile') ==  $A.get("{!$Label.c.ACP_ProfileID}"))
        {
            component.set("v.Adminprofile",true);
            component.set("v.Tenantcheck",true);
            var Action1 = component.get("c.loadorgs");   
            Action1.setParams({
                "username" : localStorage.getItem('UserSession')                
            });        
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.OrgFilter",results);
                }
                
            }); 
            $A.enqueueAction(Action1);
        }
        else{
            component.set("v.Tenantcheck",false);    
            component.set("v.Adminprofile",false);
        }
        
        component.set("v.Tenantid",localStorage.getItem('LoggeduserTenantID'));
        //Getting Tenant Ids from LocalStorage.
        component.set("v.TenantIDs",localStorage.getItem('UserSession'));
        //alert(component.get("v.TenantIDs"));
        var action = component.get('c.getTemplateAttributesList');
        var self = this; 
        component.set("v.selectedItem","Active");
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem('UserSession'),
            "Filtervalue" : component.get("v.selectedFilterItem")
        }); 
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());                
            }
        }); 
        $A.enqueueAction(action);
       
    },
    onSingleSelectChange: function(component, event, helper) {
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value");
        //alert(selectCmp);
        	component.set("v.selectedFilterItem",selectCmp);
            var action = component.get('c.Filter');
            var selected = event.getSource().get("v.text");        
            component.set("v.selectedItem", selected)
            var self = this; 
            action.setParams({
                "counter" : (component.get("v.counter")).toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "userid" : localStorage.getItem('UserSession'),
                "Filtervalue" : selectCmp                
            });
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            }); 
            $A.enqueueAction(action); 
        },
    View: function(component, event, helper) {
        debugger;
        var action = component.get('c.Views');         
        var selected = event.getSource().get("v.text");        
        component.set("v.selectedItem", selected)
        var self = this; 
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem('UserSession'),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        }); 
        $A.enqueueAction(action); 
    },    
    Beginning: function(component, event, helper) {
        var action = component.get('c.getBeginning'); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem('UserSession'),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Previous: function(component, event, helper) {
        debugger;        
        var action = component.get('c.getPrevious'); 
        var list_size = component.get('v.list_size');        
        var counter = component.get('v.counter');
        var res = counter - list_size;  
        if(res < 0) { res = 0; }
        action.setParams({ 
            "selected" : component.get("v.selectedItem"), 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
"userid" : localStorage.getItem('UserSession') ,
            "Filtervalue" : component.get("v.selectedFilterItem")           
        });       
        action.setCallback(this, function(actionResult) {           
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){ 
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Next: function(component, event, helper) {
        debugger;
        var action = component.get('c.getNext'); 
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = list_size + counter;
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") ,
            "userid" : localStorage.getItem('UserSession'),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    End: function(component, event, helper) {
        debugger;
        var action = component.get('c.getEnd');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var total_size = component.get('v.total_size');
        var res = total_size - (total_size % list_size); 
        if(res == total_size) { res -= list_size; }
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem('UserSession'),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    sort: function(component, event, helper) {
        var action = component.get('c.SortTable'); 
        var sortfield = event.currentTarget.getAttribute("data-recId");  
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : sortfield,
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") ,
            "userid" : localStorage.getItem('UserSession'),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageSizes: function(component, event, helper) {
        debugger;
        var action = component.get('c.changelist_size');  
        var list_size = component.find("recordSize").get("v.value"); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"), 
            "list_size" : list_size,
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageOptions: function(component, event, helper) {
        debugger;
        var action = component.get('c.changePage'); 
        var showpage = component.find("pageOptions").get("v.value"); 
        var list_size = component.find("recordSize").get("v.value"); 
        var counter = (parseInt(showpage)-1)*list_size;
        action.setParams({
            "counter" : counter.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem") ,
            "userid" : localStorage.getItem('UserSession'),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    createAttribute: function(component, event, helper) { 
        component.set("v.showCreate", true);     
    },
    cancel: function(component, event, helper) {
        component.set("v.showCreate", false);
        component.set("v.validation", false);
        component.set("v.AttributeNameError", '');
        component.set("v.AttributeSizeError", '');
    }, 
    btnsucessattributes: function(component, event, helper) {
        component.set("v.sucessattributes", false);
        component.set("v.showCreate", true);
        component.set("v.validation", false);
        component.set("v.AttributeNameError", '');
        component.set("v.AttributeSizeError", '');
    },
    saveNewAttribute: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var txtAttributeName = component.find("txtAttributeName").get("v.value");
        var dropAttributeType = component.find("dropAttributeType").get("v.value");
        var dropProductType = '';
        //var dropProductType = component.find("dropProductType").get("v.value");
        //var txtAttributeSize = component.find("txtAttributeSize").get("v.value");
        var txtAttributeSize = '';
        //var txtActive = component.find("txtActive").get("v.value");
        var txtActive = true;
        var txtPrimary = component.find("txtPrimary").get("v.value");
        //var txtrbtnmanyes = component.find("rbtnmanyes").get("v.value");
        
        //var rbtnmanno = component.find("rbtnmanno").get("v.value"); 
         var onlyletter =/^[A-Za-z0-9 ]+$/;
        
        
         var dropdropMandatory = component.find("dropMandatory").get("v.value");
        var PTfunded = component.find("txtfunded").get("v.value");
        var PTunfunded = component.find("txtunfunded").get("v.value");  
        var txtrbtnmanyes = ''; 
       
        if(PTfunded == true && PTunfunded == true){
            dropProductType = 'Both';
        }
        if(PTfunded == true && PTunfunded == false){
            dropProductType = 'Funded';
        }
        if(PTfunded == false && PTunfunded == true){
            dropProductType = 'UnFunded';
        }
        if(txtAttributeName == undefined || txtAttributeName == "" || txtAttributeName == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true); 
            component.set("v.AttributeNameError", 'Input field required'); 
        }else if(!$A.util.isEmpty(txtAttributeName))
        {   
            if(!txtAttributeName.match(onlyletter))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.AttributeNameError", 'Please enter without special characters.'); 
            }
            else
            {
                component.set("v.AttributeNameError", ''); 
            }
        }   
            else
            {
                errorFlag = 'true';
                component.set("v.validation", true); 
                component.set("v.AttributeNameError", 'Input field required');  
            }
       /* if(txtAttributeSize != undefined && txtAttributeSize != "" && txtAttributeSize != null)
        {
            if(helper.checkCharecter(txtAttributeSize))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.AttributeSizeError", 'Please enter a numeric value.'); 
            } 
            else
                component.set("v.AttributeSizeError", '');
        } */
        if(dropAttributeType != "DateTime" && dropAttributeType != "Number" )
        {
            txtAttributeSize = '255';
            /*errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.AttributeSizeError", 'Input field required');*/
        }
            else 
                txtAttributeSize = '';
        
        if(txtAttributeName != undefined && txtAttributeName != "" && txtAttributeName != null && errorFlag=='false')
        {
            var action = component.get("c.CheckTemplateAttributes");
          action.setParams({ "attributeName": txtAttributeName,
                              "Tenant" : localStorage.getItem('LoggeduserTenantID'),
                              "Type" : dropProductType });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var attribute = response.getReturnValue(); 
                if(attribute != '' && attribute != undefined) {
                    if(attribute == 'Already Used') { 
                        errorFlag = 'true';
                        component.set("v.validation", true); 
                        component.set("v.AttributeNameError", 'This Attribute Name has already been used for this Tenant. Please enter a new Attribute Name.');
                    } else {
                        component.set("v.AttributeNameError", ''); 
                    }
                } else { 
                    component.set("v.AttributeNameError", ''); 
                }
                if(errorFlag == 'false') {
                    component.find("btnSaveAttribute1").set("v.disabled", true); 
                    if(dropAttributeType =='Number')
                    {
                        var txtsize = '18';
                    }else
                    {
                        var txtsize = txtAttributeSize;
                    }
                    //alert(txtsize);
                    //alert('1'+component.get("v.TenantIDs"));
                    var action1 = component.get("c.SaveAttribute");
                    action1.setParams({
                        "Name" : txtAttributeName,
                        "AttributeType" : dropAttributeType,
                        "ProductType" : dropProductType,
                        "AttributeSize" : txtsize,
                        "IsActive" : txtActive,
                        "IsPrimary" : txtPrimary,
                        "IsSecondary" : false,
                        "Ismandatory" :dropdropMandatory,
                        "Isfunded" : PTfunded,
                        "Isunfunded" : PTunfunded,
                        "TenantId": component.get("v.TenantIDs"),
                        "LoggeduserTid": localStorage.getItem("LoggeduserTenantID")
                    }); 
                    action1.setCallback(this, function(response1) {
                        var state1 = response1.getState();
                        var attributeId = response1.getReturnValue(); 
                        debugger;
                        component.find("btnSaveAttribute1").set("v.disabled", false);
                        if(attributeId != '' && attributeId != undefined) {
                            if(attributeId == 'Already Used'){ 
                                component.set("v.validation", true);                     
                                component.set("v.AttributeNameError", 'This Attribute Name has already been used. Please enter a new Attribute Name.');
                            } else {                                
                                component.set("v.showCreate", false);
                                component.set("v.sucessattributes", true);
                                
                            }
                        }
                        else{ 
                            component.set("v.validation", false); 
                            component.set("v.showCreate", false);
                            component.set("v.AttributeSizeError", '');
                        }
                        
                    });
                    $A.enqueueAction(action1);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    saveAttribute: function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var txtAttributeName = component.find("txtAttributeName").get("v.value");
        var dropAttributeType = component.find("dropAttributeType").get("v.value");
        var dropProductType = '';
        //var dropProductType = component.find("dropProductType").get("v.value");        
        //var txtAttributeSize = component.find("txtAttributeSize").get("v.value");
        var txtAttributeSize = '';
        //var txtActive = component.find("txtActive").get("v.value");
        var txtActive = true;
        var txtPrimary = component.find("txtPrimary").get("v.value");
        //var txtrbtnmanyes = component.find("rbtnmanyes").get("v.value");
        //var txtSecondary = component.find("txtSecondary").get("v.value"); 
        //var txtrbtnmanyes = component.find("rbtnmanyes").get("v.value");
        var onlyletter =/^[A-Za-z0-9 ]+$/;
        var PTfunded = component.find("txtfunded").get("v.value");
        var PTunfunded = component.find("txtunfunded").get("v.value");   
        var txtrbtnmanyes = ''; 
         var dropdropMandatory = component.find("dropMandatory").get("v.value");
        if(PTfunded == true && PTunfunded == true){
            dropProductType = 'Both';
        }
        if(PTfunded == true && PTunfunded == false){
            dropProductType = 'Funded';
        }
        if(PTfunded == false && PTunfunded == true){
            dropProductType = 'UnFunded';
        }
        if(txtAttributeName == undefined || txtAttributeName == "" || txtAttributeName == null)
        {
            errorFlag = 'true';
            component.set("v.validation", true); 
            component.set("v.AttributeNameError", 'Input field required'); 
        }else if(!$A.util.isEmpty(txtAttributeName))
        {   
            if(!txtAttributeName.match(onlyletter))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.AttributeNameError", 'Please enter without special characters.'); 
            }
            else
            {
                component.set("v.AttributeNameError", ''); 
            }
        }   
            else
            {
                errorFlag = 'true';
                component.set("v.validation", true); 
                component.set("v.AttributeNameError", 'Input field required');  
            }        
        /*if(txtAttributeSize != undefined && txtAttributeSize != "" && txtAttributeSize != null)
        {
            if(helper.checkCharecter(txtAttributeSize))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.AttributeSizeError", 'Please enter a numeric value.'); 
            } 
            else
                component.set("v.AttributeSizeError", '');
        } */
        if(dropAttributeType != "DateTime" && dropAttributeType != "Number" )
        {
            txtAttributeSize = '255';
           /* errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.AttributeSizeError", 'Input field required'); */
        }
            else 
                txtAttributeSize = '';
        
        if(txtAttributeName != undefined && txtAttributeName != "" && txtAttributeName != null && errorFlag=='false')
        {
            var action = component.get("c.CheckTemplateAttributes");
            action.setParams({ "attributeName": txtAttributeName,
                              "Tenant" : localStorage.getItem('LoggeduserTenantID'),
                              "Type" : dropProductType });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var attribute = response.getReturnValue(); 
                if(attribute != '' && attribute != undefined) {
                    if(attribute == 'Already Used') { 
                        errorFlag = 'true';
                        component.set("v.validation", true); 
                        component.set("v.AttributeNameError", 'This Attribute Name has already been used for this Tenant. Please enter a new Attribute Name.');
                    } else {
                        component.set("v.AttributeNameError", ''); 
                    }
                } else { 
                    component.set("v.AttributeNameError", ''); 
                }
                if(errorFlag == 'false') {
                    component.find("btnSaveAttribute").set("v.disabled", true);                    
                    var action1 = component.get("c.SaveAttribute");
                    if(dropAttributeType =='Number')
                    {
                        var txtsize = '18';
                    }else
                    {
                        var txtsize = txtAttributeSize;
                    }
                    //alert(txtsize);
                    // alert(component.get("v.TenantIDs"));
                    action1.setParams({
                        "Name" : txtAttributeName,
                        "AttributeType" : dropAttributeType,
                        "ProductType" : dropProductType,
                        "AttributeSize" : txtsize,
                        "IsActive" : txtActive,
                        "IsPrimary" : txtPrimary,
                        "IsSecondary" : false,
                        "Ismandatory" :dropdropMandatory,
                        "Isfunded" : PTfunded,
                        "Isunfunded" : PTunfunded,
                        "TenantId": component.get("v.TenantIDs"),
                        "LoggeduserTid": localStorage.getItem("LoggeduserTenantID")
                    }); 
                    action1.setCallback(this, function(response1) {
                        var state1 = response1.getState();
                        var attributeId = response1.getReturnValue(); 
                        debugger;
                        component.find("btnSaveAttribute").set("v.disabled", false);
                        if(attributeId != '' && attributeId != undefined) {
                            if(attributeId == 'Already Used'){ 
                                component.set("v.validation", true);                     
                                component.set("v.AttributeNameError", 'This Attribute Name has already been used. Please enter a new Attribute Name.');
                            } else {
                                var site = $A.get("{!$Label.c.Org_URL}");
                                window.location = '/'+site+'/s/template-attribute-detail?attributeId='+ attributeId;
                            }
                        }
                        else{ 
                            
                            component.set("v.validation", false); 
                            $A.get('e.force:refreshView').fire();
                            component.set("v.AttributeSizeError", '');
                            
                        }
                        
                    });
                    $A.enqueueAction(action1);
                }
            });
            $A.enqueueAction(action);
        }
    }, 
    changeAttributeType: function(component, event, helper) {   
        debugger;
        var dropAttributeType = component.find("dropAttributeType").get("v.value");
        if(dropAttributeType == "DateTime" || dropAttributeType == "Number"){
            component.find("txtAttributeSize").set("v.value", '');
            component.find("txtAttributeSize").set("v.disabled", true); 
            component.set("v.AttributeSizeError", '');
        }
        else{component.find("txtAttributeSize").set("v.disabled", false); 
            }
    },
    changePrimary: function(component, event, helper) {  
        debugger;
        var txtPrimary = component.find("txtPrimary").get("v.value");
        if(txtPrimary == true)  
            component.find("txtSecondary").set("v.value", false); 
        else
            component.find("txtSecondary").set("v.value", true); 
    },
    changeSecondary: function(component, event, helper) {   
        debugger;
        var txtSecondary = component.find("txtSecondary").get("v.value");
        if(txtSecondary == true)  
            component.find("txtPrimary").set("v.value", false); 
        else
            component.find("txtPrimary").set("v.value", true); 
    },
})