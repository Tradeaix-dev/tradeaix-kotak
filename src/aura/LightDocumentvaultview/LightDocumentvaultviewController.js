({
    doinit : function(component, event, helper){
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        }; 
                var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        if(dd > 9){var date = dd;}else{var date = '0'+dd;}
         if(mm > 9){var month = mm;}else{var month = '0'+mm;}
        component.set("v.currentDate",yyyy+'-'+month+'-'+date);
        var folderId = getUrlParameter('fid');   
        component.set("v.loggeduserId",localStorage.getItem("IDTenant"));
        if(folderId == undefined || folderId == ""){
            var Action1 = component.get('c.loadfolders'); 
            Action1.setParams({            
                "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
                "userId" :localStorage.getItem("IDTenant")
            }); 
        }else{
            component.set("v.FolderId",folderId);
            var Action1 = component.get('c.selectedloadfolders'); 
            Action1.setParams({            
                "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID") ,
                "parentfolder":folderId,
                "userId" :localStorage.getItem("IDTenant")
            }); 
        }
       
        Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.Banks",results);
                    component.set("v.userList",results.userlist);
                    component.set("v.Foldersection", false);
                    component.set("v.Filedetailsection", true);
                    component.set("v.FolderNameselected",results.selectedfoldername);
                }
            }); 
            $A.enqueueAction(Action1);
    },
    ContentVersion: function(component, event, helper) { 
        debugger;
        var id = event.currentTarget.dataset.id; //it will return thisDiv
        component.set("v.FolderNameselected",id);
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Document Vault';
        if(localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.ACP_ProfileID}"))
        {
            component.set("v.Tenantcheck",true);
            var Action1 = component.get("c.loadorgs");   
            Action1.setParams({
                "username" : localStorage.getItem('UserSession')                
            });        
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.OrgFilter",results);
                }
                
            }); 
            $A.enqueueAction(Action1);
        }
        else{
            component.set("v.Tenantcheck",false);                        
        }
        //alert(localStorage.getItem("IDTenant"));
        var action = component.get('c.getContentVersionList');
        var self = this;
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
			"LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
            
        }); 
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
                component.set("v.isFilterA", false);
                var nextAction = component.get("c.secondarymarketingflag"); 
                debugger;
                nextAction.setCallback(this, function(actionResult) {
                    var results = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        debugger;
                        component.set("v.issecondaryMarket",results)
                        component.set("v.Foldersection", false);
                        component.set("v.Filedetailsection", true);
                    }
                });
                $A.enqueueAction(nextAction);
                
                //$A.get('e.force:refreshView').fire();
            }
        }); 
    }, 
    Beginning: function(component, event, helper) {
        var action = component.get('c.getBeginning'); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Previous: function(component, event, helper) {
        debugger;        
        var action = component.get('c.getPrevious'); 
        var list_size = component.get('v.list_size');        
        var counter = component.get('v.counter');
        var res = counter - list_size;  
        if(res < 0) { res = 0; }
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID") ,
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });       
        action.setCallback(this, function(actionResult) {           
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){ 
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Next: function(component, event, helper) {
        var action = component.get('c.getNext'); 
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = list_size + counter;
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    End: function(component, event, helper) {
        var action = component.get('c.getEnd');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var total_size = component.get('v.total_size');
        var res = total_size - (total_size % list_size); 
        if(res == total_size) { res -= list_size; }
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    sort: function(component, event, helper) {
        var action = component.get('c.SortTable'); 
        var sortfield = event.currentTarget.getAttribute("data-recId");  
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : sortfield,
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
            
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageSizes: function(component, event, helper) {
        var action = component.get('c.changelist_size');  
        var list_size = component.find("recordSize").get("v.value"); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),           
            "list_size" : list_size,
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageOptions: function(component, event, helper) {
        var action = component.get('c.changePage'); 
        var showpage = component.find("pageOptions").get("v.value"); 
        var list_size = component.find("recordSize").get("v.value"); 
        var counter = (parseInt(showpage)-1)*list_size;
        action.setParams({
            "counter" : counter.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    refreshModel: function (component, event, helpler) {
        debugger;
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        component.set("v.FileUploadMsg", 'File(s) have been uploaded successfully');
        component.set("v.FileUploadMsgSorE", 'Success');
        component.set("v.FileUpload", true);
        var a1 = component.get('c.ContentVersion');
        $A.enqueueAction(a1);
                $A.get('e.force:refreshView').fire();
    },
    
    ChooseFile : function(component, event) {     
        debugger;
        component.set("v.FileUploadMsg", 'Please choose the file(s)');
        component.set("v.FileUploadMsgSorE", 'Error');
        component.set("v.FileUpload", true); 
    },
    uploadfilepopup: function(component, event) {     
        debugger;
        component.set("v.opendocvault", true); 
    },
    btncancelfor: function(component, event) {     
        debugger;
        component.set("v.opendocvault", false); 
    },
    ChooseFailure : function(component, event) {        
        component.set("v.FileUploadMsg", 'An error has occurred. Please contact us at\nLoanParticipationBeta@cunamutual.com.');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    fileExtension : function(component, event) {        
        component.set("v.FileUploadMsg", '(.exe,.rar,.js,.zip) file format not allowed to upload');
        component.set("v.FileUploadMsgSorE", 'Error');        
        component.set("v.FileUpload", true); 
    },
    LoaderDisplay : function(component, event) {
        var img = component.find("imgloading");
        $A.util.removeClass(img,'slds-hide');
        $A.util.addClass(img,'slds-show');  
    },
    FileUploadcancel : function(component, event) {
        var img = component.find("imgloading");
        $A.util.addClass(img,'slds-hide'); 
        $A.util.removeClass(img,'slds-show');
        var img1 = component.find("imgloadingDue");
        $A.util.addClass(img1,'slds-hide'); 
        $A.util.removeClass(img1,'slds-show');
        component.set("v.FileUpload", false); 
        component.set("v.opendocvault", false);
    },
    fileextensionfailure : function(component, event) {
        debugger;
        component.set("v.opendocvault", false);
        component.set("v.fileextensionwarning", true);
        
    },
    btnFilter: function(component) { 
        debugger;        
        var action = component.get('c.getFilter');
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            console.log("state" + state);
            if(state === "SUCCESS"){
                var result = actionResult.getReturnValue(); 
                component.set("v.filterObject", result);
                component.set("v.filterObject.CreateddateTo__c", result.CreateddateTo__c);
                component.set("v.filterObject.CreateddateFrom__c", result.CreateddateFrom__c);
                component.set("v.Filter", true);
                component.set("v.MinFilter", false);
                window.setTimeout($A.getCallback(function(){ 
                    //Make the DIV element draggagle:
                    dragElement(document.getElementById("mydiv")); 
                    
                    function dragElement(elmnt) {  
                        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
                        if (document.getElementById(elmnt.id + "header")) {
                            /* if present, the header is where you move the DIV from:*/
                            document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
                        } else {
                            /* otherwise, move the DIV from anywhere inside the DIV:*/
                            elmnt.onmousedown = dragMouseDown;
                        }
                        
                        function dragMouseDown(e) { 
                            e = e || window.event;
                            e.preventDefault();
                            // get the mouse cursor position at startup:
                            pos3 = e.clientX;
                            pos4 = e.clientY;
                            document.onmouseup = closeDragElement;
                            // call a function whenever the cursor moves:
                            document.onmousemove = elementDrag;
                        }
                        
                        function elementDrag(e) { 
                            e = e || window.event;
                            e.preventDefault();
                            // calculate the new cursor position:
                            pos1 = pos3 - e.clientX;
                            pos2 = pos4 - e.clientY;
                            pos3 = e.clientX;
                            pos4 = e.clientY;
                            // set the element's new position:
                            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
                        }
                        
                        function closeDragElement() { 
                            /* stop moving when mouse button is released:*/
                            document.onmouseup = null;
                            document.onmousemove = null;
                        }
                    } 
                }), 500);
            } 
        });
        $A.enqueueAction(action);        
    }, 
    cancelFilter : function(component, event) {
        debugger;
        component.set("v.Filter", false);         
    },     
    minFilter: function(component, event) {
        component.set("v.Filter", false);
        component.set("v.MinFilter", true);        
        window.setTimeout($A.getCallback(function(){ 
            //Make the DIV element draggagle:
            dragElement(document.getElementById("mydiv1")); 
            
            function dragElement(elmnt) {  
                var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
                if (document.getElementById(elmnt.id + "header")) {
                    /* if present, the header is where you move the DIV from:*/
                    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
                } else {
                    /* otherwise, move the DIV from anywhere inside the DIV:*/
                    elmnt.onmousedown = dragMouseDown;
                }
                
                function dragMouseDown(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // get the mouse cursor position at startup:
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    document.onmouseup = closeDragElement;
                    // call a function whenever the cursor moves:
                    document.onmousemove = elementDrag;
                }
                
                function elementDrag(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // calculate the new cursor position:
                    pos1 = pos3 - e.clientX;
                    pos2 = pos4 - e.clientY;
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    // set the element's new position:
                    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
                }
                
                function closeDragElement() { 
                    /* stop moving when mouse button is released:*/
                    document.onmouseup = null;
                    document.onmousemove = null;
                }
            } 
        }), 500);
    }, 
    maxFilter: function(component, event) { 
        component.set("v.MinFilter", false); 
        component.set("v.Filter", true);
        window.setTimeout($A.getCallback(function(){ 
            //Make the DIV element draggagle:
            dragElement(document.getElementById("mydiv")); 
            
            function dragElement(elmnt) {  
                var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
                if (document.getElementById(elmnt.id + "header")) {
                    /* if present, the header is where you move the DIV from:*/
                    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
                } else {
                    /* otherwise, move the DIV from anywhere inside the DIV:*/
                    elmnt.onmousedown = dragMouseDown;
                }
                
                function dragMouseDown(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // get the mouse cursor position at startup:
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    document.onmouseup = closeDragElement;
                    // call a function whenever the cursor moves:
                    document.onmousemove = elementDrag;
                }
                
                function elementDrag(e) { 
                    e = e || window.event;
                    e.preventDefault();
                    // calculate the new cursor position:
                    pos1 = pos3 - e.clientX;
                    pos2 = pos4 - e.clientY;
                    pos3 = e.clientX;
                    pos4 = e.clientY;
                    // set the element's new position:
                    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
                    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
                }
                
                function closeDragElement() { 
                    /* stop moving when mouse button is released:*/
                    document.onmouseup = null;
                    document.onmousemove = null;
                }
            } 
        }), 500);
    }, 
    GoSearch : function(component, event,helper) { 
        debugger;
        var filter = component.get("v.searchKeyword"); 
        console.log('@filter@'+filter);
        var action = component.get('c.saveFilter'); 
        action.setParams({ 
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "filter" : filter,
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                helper.setGridOption(component, actionResult.getReturnValue());
                debugger;
                component.set("v.filterObject", filter);
                component.set("v.Filter", false);   
            } 
        });
        $A.enqueueAction(action);
    }, 
    applyFilter : function(component, event,helper) { 
        debugger;
        var stardateTime=component.find("Createddatefrom").get("v.value");
        var enddateTime=component.find("Createddateto").get("v.value");
        debugger;
        if((stardateTime==null && enddateTime!=null) || (stardateTime!=null && enddateTime==null)) {
            debugger;
            component.set("v.EnddateErrormsg", "Created Date cannot be a blank.");
            return null;
        }
        var filter = component.get("v.filterObject");
        console.log('@filter@'+filter);
        var action = component.get('c.alyFilter'); 
        var self = this;
        action.setParams({ 
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),            
            "filter" : filter,
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) { 
            
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                helper.setGridOption(component, actionResult.getReturnValue());
                debugger;
                component.set("v.filterObject", filter);
                component.set("v.Filter", false); 
            }               
        });
        $A.enqueueAction(action);
        
    },
    onSingleSelectChange: function(component, event, helper) {
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value");
        //alert(selectCmp);
        	component.set("v.selectedFilterItem",selectCmp);
            var action = component.get('c.Filter');
            var selected = event.getSource().get("v.text");        
            component.set("v.selectedItem", selected)
            var self = this; 
            action.setParams({
                "counter" : (component.get("v.counter")).toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"), 
                "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
                "LoggedUserId" : localStorage.getItem("IDTenant"),
                "Filtervalue" : selectCmp                
            });
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            }); 
            $A.enqueueAction(action); 
        },
    resetFilter : function(component, event) { 
        debugger;
        component.set("v.filterObject.CreateddateTo__c", null);
        component.set("v.filterObject.CreateddateFrom__c", null);
        component.set("v.searchKeyword", null);
        component.set("v.isFilterA", false);
        
        
    },
     btnaddFolder : function(component, event) { 
        debugger;
        component.set("v.opencreatefolder", true);
    },
    cancelcreatefolder:function(component, event) { 
        debugger;
        component.set("v.opencreatefolder", false);
    },
    savefolder:function(component, event, helper) {
        debugger;
        var ErrorFlag = 'False';
        var alphanumeric =/^[a-zA-Z0-9\s]+$/;
        var name = component.find("foldername").get("v.value");
        if(name == undefined || name == ""){
            ErrorFlag ='true';           
            component.set("v.foldernameError",'Input field required');
        }else if(!$A.util.isEmpty(name)){
            if(!name.match(alphanumeric)){
                ErrorFlag = 'true';               
                component.set("v.foldernameError",'Name not allowed special characters');
            }else if(name.length > 150 ){
                ErrorFlag = 'true'; 
                component.set("v.foldernameError",'Name not allowed more than 150 characters');
            }else{
                component.set("v.foldernameError",'');
            }
        }
        else{
            component.set("v.foldernameError",'');
        }        
        if(ErrorFlag =='False'){ 
            if(component.get("v.FolderId") !=null){
                var action = component.get('c.addnewfolderwithparent');
                action.setParams({
                    "foldername" : name,
                    "LoggedUserId" : localStorage.getItem("IDTenant"),
                    "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
                    "parentfolder": component.get("v.FolderId")
                });
            }else{
                var action = component.get('c.addnewfolder');
                action.setParams({
                    "foldername" : name,
                    "LoggedUserId" : localStorage.getItem("IDTenant"),
                    "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID")    
                });
            }           
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    if(results == 'Already Used') { 
                        component.set("v.foldernameError",'This Folder Name already Exist. Please enter a new Folder Name.');
                    }else{
                        component.set("v.opencreatefolder", false);  
                        $A.get('e.force:refreshView').fire();
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': 'Folder Created Successfully',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();
                    }
                }
            }); 
            $A.enqueueAction(action); 
        } 
    },
    redirecthome: function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/docvalutview?fud=';
    },
    sharefolder : function(component, event, helper) {
        debugger; 
        var index = event.target.id;
        component.set("v.selectedfileId",index);
        component.set("v.shareuserview", true);
        var Action = component.get('c.selectedfilesharing'); 
        Action.setParams({            
            "selectedId" : index           
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                component.set("v.Foldernameforshow",results.Filefoldersname);
                component.set("v.Foldershareaccess",results.FileshareFlag);
                if(results.FileshareFlag=='Public'){
                    component.find("r0").set("v.value",true);  
                }
                else if(results.FileshareFlag=='private'){
                    component.find("r1").set("v.value",true);  
                }else if(results.FileshareFlag=='Share'){
                    
                    component.set("v.shareview", true);
                    component.find("r2").set("v.value",true);  
                } else{
                    component.find("r1").set("v.value",true);  
                }
                

                component.set("v.userList",results.userlist);               
            }
        }); 
        $A.enqueueAction(Action); 
    },
    sharefile : function(component, event, helper) {
        debugger;
        var index = event.target.id;  
         component.set("v.selectedfileId",index);        
                component.set("v.shareuserview", true);
        var Action = component.get('c.selectedfilesharing'); 
        Action.setParams({            
            "selectedId" : index           
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                component.set("v.Foldernameforshow",results.Filefoldersname);
                component.set("v.Fileshareaccess",results.FileshareFlag);
                if(results.FileshareFlag=='Public'){
                    component.find("r0").set("v.value",true);  
                }
                else if(results.FileshareFlag=='private'){
                    component.find("r1").set("v.value",true);  
                }else if(results.FileshareFlag=='Share'){
                    
                    component.set("v.shareview", true);
                    component.find("r2").set("v.value",true);  
                } else{
                    component.find("r1").set("v.value",true);  
                }
                component.set("v.userList",results.userlist);
               
            }
        }); 
        $A.enqueueAction(Action); 
        
    },
    cancelshare : function(component, event, helper) {
        debugger;
        component.set("v.shareuserview", false);
        $A.get('e.force:refreshView').fire();
    },
    openfolder : function(component, event, helper) {
        debugger;
        var id = event.currentTarget.dataset.id;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/docvalutview?fid='+id;
      /*  var id = event.currentTarget.dataset.id; //it will return thisDiv
        component.set("v.FolderNameselected",id);   
        var Action1 = component.get('c.loadfolderstest'); 
        Action1.setParams({            
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID")            
        }); 
        Action1.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Banks",results);
                component.set("v.Foldersection", false);
                component.set("v.Filedetailsection", true);
                component.set("v.openuploadsection",true);
            }
        }); 
        $A.enqueueAction(Action1); */
    },
    handleSelectAllusers : function(component, event, helper) {
        debugger;
        var getID = component.get("v.userList");
        var checkvalue = component.find("selectAll").get("v.value");        
        var checkContact = component.find("checkusers"); 
        if(checkvalue == true){
            for(var i=0; i<checkContact.length; i++){
                checkContact[i].set("v.value",true);
            }
        }
        else{ 
            for(var i=0; i<checkContact.length; i++){
                checkContact[i].set("v.value",false);
            }
        }
    },
    resetfilterser: function(component, event, helper) {
            component.set("v.resetfilter",false);
            component.find("searchtxtfile").set("v.value",'');
            $A.get('e.force:refreshView').fire();
            debugger;  },
    saveshareusers: function(component, event, helper) {
        debugger;
        var selectedUsers = [];
        var checkvalue = component.find("checkusers");        
        if(!Array.isArray(checkvalue)){
            if (checkvalue.get("v.value") == true) {
                selectedUsers.push(checkvalue.get("v.text"));
            }
        }else{
            for (var i = 0; i < checkvalue.length; i++) {
                if (checkvalue[i].get("v.value") == true) {
                    selectedUsers.push(checkvalue[i].get("v.text"));
                }
            }
        }       
        console.log('selectedUsers-' + selectedUsers);
        var action = component.get('c.foldersharing');        
        action.setParams({    
            "selectedId" : component.get("v.selectedfileId"),
            "selectedUsers" : selectedUsers,
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant")            
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(results === "Success"){  
                component.set("v.shareuserview", false);
            }
        }); 
        $A.enqueueAction(action); 
    },
    onGroup :function(component, event, helper) {
        debugger;
        var selected = event.getSource().get("v.label");
        if(selected == 'Share with others'){
            component.set("v.shareview", true);
        }else{
            component.set("v.shareview", false);
        }
    },
    onShareothers : function(component, event, helper) {
        debugger;
        component.set("v.shareview", true);
        component.find("r2").set("v.value",true);
        //alert(component.get("v.Fileshareaccess"));
        //alert(component.get("v.Foldershareaccess"));
    },
    onPublic: function(component, event, helper) {
        debugger;
        component.find("r0").set("v.value",true); 
        component.set("v.shareview", false);
        component.set("v.shareuserview", false);  
        if(component.get("v.Foldershareaccess")=='Private'){
         component.set("v.publicconfirmation", true);   
        }
        else{
             component.set("v.sharepublicconfirmation", true);   
        }
        
        
       /*  var action = component.get('c.filesharetopublic');   
        action.setParams({    
            "selectedId" : component.get("v.selectedfileId"), 
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant")  
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                if(results == 'Success'){
                    component.set("v.shareuserview", false);
                }
            }
        }); 
        $A.enqueueAction(action); */
         
    },
    onPrivate : function(component, event, helper) {
        debugger;
        component.find("r1").set("v.value",true); 
        component.set("v.shareview", false);
        component.set("v.shareuserview", false);
        //alert(component.get("v.Fileshareaccess"));
        //alert(component.get("v.Foldershareaccess"));
        if(component.get("v.Foldershareaccess")=='Public'){
        component.set("v.privateconfirmation",true);
        }else{
         component.set("v.shareprivateconfirmation",true);
        }
            
    },
    
    Delcancel : function(component, event, helper) {
        debugger;
         component.set("v.delcconfirmation", false);
    },
     Delfilecancel : function(component, event, helper) {
        debugger;
         component.set("v.delcfileconfirmation", false);
    },
     confirmdelfile : function(component, event, helper) {
        debugger;
        var index = event.target.id;
        var Action = component.get('c.selectedfiledelete'); 
        Action.setParams({            
            "selectedId" : component.get("v.FileIdforDel")           
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                if(results == 'Success'){
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'File Deleted Successfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
            }
        }); 
        $A.enqueueAction(Action);
    },
    
    deletefile : function(component, event, helper) {
        debugger;
         component.set("v.FileIdforDel",event.target.id);
        
        var Action = component.get('c.SelectFileName'); 
        Action.setParams({            
            "selectedId" : component.get("v.FileIdforDel")         
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                
                component.set("v.Filenameforshow",results);
                component.set("v.delcfileconfirmation", true);
            }
        }); 
        $A.enqueueAction(Action);
         
    },
    deletefolder : function(component, event, helper) {
        debugger;
        component.set("v.FolderIdforDel",event.target.id);
        var Action = component.get('c.SelectFolderName'); 
        Action.setParams({            
            "selectedId" : component.get("v.FolderIdforDel")         
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                
                component.set("v.Foldernameforshow",results);
                component.set("v.delcconfirmation", true);
            }
        }); 
        $A.enqueueAction(Action);
        
    },
      confirmdelfolder : function(component, event, helper) {
        debugger;
        var Action = component.get('c.selectedfolderdelete'); 
        Action.setParams({            
            "selectedId" : component.get("v.FolderIdforDel")           
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                if(results == 'Success'){
                    $A.get('e.force:refreshView').fire();
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Folder Deleted Successfully',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
            }
        }); 
        $A.enqueueAction(Action);
    },
    revokeshare : function(component, event, helper) {
        debugger;
        var entryId = event.currentTarget.dataset.id;      
        var action = component.get('c.deleteshare');        
        action.setParams({    
            "selectedId" : entryId  ,
            "fileId" : component.get("v.selectedfileId")
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                component.set("v.userList",results.userlist);
            }
        }); 
        $A.enqueueAction(action); 
        
    },
    handleUserRecord: function(component, event, helper) {
        debugger;
        var Action = component.get('c.selectedfilesharing'); 
        Action.setParams({            
            "selectedId" : component.get("v.selectedfileId")           
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                component.set("v.userList",results.userlist);
                component.set("v.selectedfileId",index);        
                component.set("v.shareuserview", true);
            }
        }); 
        $A.enqueueAction(Action); 
        /*var Action = component.get('c.loadfolders'); 
        Action.setParams({            
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "userId" :localStorage.getItem("IDTenant")
        }); 
        Action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){ 
                component.set("v.userList",results.userlist);
            }
        }); 
        $A.enqueueAction(Action);*/
        
    },    
    addtag: function(component, event, helper) {
        debugger;
        var index = event.target.id;       
        component.set("v.selectedfileId", index);
        component.set("v.tagnotes", true);
    },
    tagCancel: function(component, event, helper) {
        debugger;
		component.set("v.tagnotes", false);
    },
    tagNotesSave: function(component, event, helper) {
        debugger;
        var ErrorFlag = 'False';
        var name = component.find("body").get("v.value");
        if(name == undefined || name == ""){
            ErrorFlag ='true';           
            component.set("v.saveNote",'Input field required');
        }else{
            component.set("v.saveNote",'');
        }
        if(ErrorFlag =='False'){ 
            var Action = component.get('c.filetagupdate'); 
            Action.setParams({            
                "selectedId" : component.get("v.selectedfileId"),
                "notes" :name
            }); 
            Action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state === "SUCCESS"){ 
                    if(results == 'Success'){
                        component.set("v.tagnotes", false);
                        $A.get('e.force:refreshView').fire();
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': 'Tag Created Successfully',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();
                    }
                   
                }
            }); 
            $A.enqueueAction(Action); 
            
        }
        
    },
    
    searchfiles:function(component, event, helper) {
        component.set("v.resetfilter",true);
        debugger;
        var searchtext = component.find("searchtxtfile").get("v.value");  
        if(searchtext == undefined || searchtext == ''){
            var site = $A.get("{!$Label.c.Org_URL}");
            window.location = '/'+site+'/s/docvalutview';
        }else{
            var Action = component.get("c.getFileSearch"); 
            Action.setParams({             
                "seachtext" : searchtext,
                "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
                "LoggedUserId" : localStorage.getItem("IDTenant")  
            });
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state === "SUCCESS"){               
                    component.set("v.Banks",results);
                    component.set("v.userList",results.userlist);           
                }
            });
            $A.enqueueAction(Action);
        }
        
    },
    privatesharecancel : function(component, event, helper) {      
        debugger;
        component.set("v.privateconfirmation", false);
    },
     privatesharecancel1 : function(component, event, helper) {      
        debugger;
        component.set("v.shareprivateconfirmation", false);
    },
    onlyprivate: function(component, event, helper) {      
        debugger;        
        var Action = component.get("c.onlyprivateshare"); 
        Action.setParams({             
            "selectedId" : component.get("v.selectedfileId"),
            "userId" : localStorage.getItem("IDTenant")    
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){               
                component.set("v.shareuserview", false);
                component.set("v.privateconfirmation", false);
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(Action);
        
    },
    publicsharecancel:function(component, event, helper) {      
        debugger;  
        component.set("v.publicconfirmation", false);
    },
     publicsharecancel1:function(component, event, helper) {      
        debugger;  
        component.set("v.sharepublicconfirmation", false);
    },
    publicsharefile:function(component, event, helper) {      
        debugger;  
        var action = component.get('c.filesharetopublic');   
        action.setParams({    
            "selectedId" : component.get("v.selectedfileId"), 
            "LoggeduserTenantID" : localStorage.getItem("LoggeduserTenantID"),
            "LoggedUserId" : localStorage.getItem("IDTenant")  
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                if(results == 'Success'){
                    component.set("v.publicconfirmation", false);
                    $A.get('e.force:refreshView').fire();
                }
            }
        }); 
        $A.enqueueAction(action);
        
    },
    filewarningclose : function(component, event, helper) {      
        debugger;  
        component.set("v.fileextensionwarning", false);
    },
    setExpiration : function(component, event, helper) {      
        debugger;
        document.getElementById('tr_'+event.target.id).style.display = 'table-row';
        component.set("v.showExpirationPicker", true);
        component.set("v.targetID", event.target.id);
    },
    
    cancelExpiration : function(component, event, helper) { 
        debugger;
        document.getElementById('tr_'+event.currentTarget.dataset.id).style.display = 'none';
    },
    saveExpDtChanges  : function(component, event, helper) { 
        debugger;
        //alert('saveid'+component.get("v.saveid"));
        //alert('savedate'+component.get("v.savedate"));
        //alert('saveopt'+component.get("v.saveopt"));
        var expdate='';
        if(component.get("v.saveopt") == 'CustDt2'){
            expdate=component.get("v.savedate");
        }else if(component.get("v.saveopt") == '7'){
            var result = new Date();
            result.setDate(result.getDate() + 7);
            var today = $A.localizationService.formatDate(result, "YYYY-MM-DD");
            expdate= today;
        }else if(component.get("v.saveopt") == '30'){  
            var result = new Date();
            result.setDate(result.getDate() + 30);
            var today = $A.localizationService.formatDate(result, "YYYY-MM-DD");
            expdate= today;
        }else{
            expdate='';
        }
        var action = component.get('c.updateexpdate');   
        action.setParams({    
            "Fsharing" : component.get("v.saveid"), 
            "ExpDate" : expdate
            
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                if(results == 'Success'){
                    var closeid = component.get("v.saveid").substring(11,29);
                    document.getElementById('tr_'+closeid).style.display = 'none';
                    //$A.get('e.force:refreshView').fire();
                }
            }
        }); 
        $A.enqueueAction(action);
        
    },expdate: function(component, event, helper) {
        debugger; 
        var d= event.target.id;
        var DDvalue = document.getElementById(d).value;
        component.set("v.saveid",d);
        component.set("v.savedate",DDvalue);
    },
    expDTOnChange : function(component, event, helper) {
        debugger; 
        var id=component.get("v.targetID");
        component.set("v.saveid",'expCustDate'+id);
         component.set("v.saveopt",event.getSource().get("v.value"));
        if (event.getSource().get("v.value") == "CustDt2") 
        {
            //component.set("v.showCustDatepk", true);
            document.getElementById('trExpCDT_'+id).style.display = 'block';            
            
        }else{
             document.getElementById('trExpCDT_'+id).style.display = 'none'; 
        }
        //do something else
    },
    
    setSharefileExpDt:function(component, event, helper) {      
        debugger;  
        var action = component.get('c.setSharefileExpDt');   
        action.setParams({    
            
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){  
                if(results == 'Success'){
                    $A.get('e.force:refreshView').fire();
                }
            }
        }); 
        $A.enqueueAction(action);
        
    },
    
    
    
})