({afterScriptsLoaded : function(component, evt, hlp) {
    debugger;
    var action = component.get('c.loadAccounts');
    var getUrlParameter = function getUrlParameter(sParam) {
        try{
            var searchString = document.URL.split('?')[1];
            var sPageURL = decodeURIComponent(searchString),
                sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');                 
                if (sParameterName[0] === sParam)  {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        } catch(ex){}
    };
    
    var bankids = getUrlParameter('bankId');
    component.set("v.bankId",bankids);
    action.setParams({
        "BID" : bankids          
    });
    action.setCallback(this, function(callbackResult) {
        if(callbackResult.getState()=='SUCCESS') {
            component.set("v.ERoles",callbackResult.getReturnValue());
        }
    }); 
    $A.enqueueAction(action);
}, 
  DeleteEU:function(component, event) {
      debugger;
      var action = component.get('c.DeleteEUs');
      action.setParams({
          "EUId" : event.target.id          
      });
      action.setCallback(this, function(actionResult) { 
          debugger;
          var state = actionResult.getState(); 
          var result = actionResult.getReturnValue();
          if(state === "SUCCESS"){
              $A.get('e.force:refreshView').fire();
          }
      });
      $A.enqueueAction(action);
  },
  
  ADDEU:function(component, event) {
      var action = component.get('c.ADDEUs');
      action.setParams({
          "EUId" : event.target.id          
      });
      action.setCallback(this, function(actionResult) { 
          debugger;
          var state = actionResult.getState(); 
          var result = actionResult.getReturnValue();
          if(state === "SUCCESS"){
              $A.get('e.force:refreshView').fire();
          }
      });
      $A.enqueueAction(action);
  },
  DeleteER:function(component, event) {
      debugger;
      var action = component.get('c.DeleteERs');
      action.setParams({
          "ERId" : event.target.id          
      });
      action.setCallback(this, function(actionResult) { 
          debugger;
          var state = actionResult.getState(); 
          var result = actionResult.getReturnValue();
          if(state === "SUCCESS"){
              $A.get('e.force:refreshView').fire();
          }
      });
      $A.enqueueAction(action);
  },
  
  ADDER:function(component, event) {
      var action = component.get('c.ADDERs');     
      action.setCallback(this, function(actionResult) { 
          debugger;
          var state = actionResult.getState(); 
          var result = actionResult.getReturnValue();
          if(state === "SUCCESS"){
              $A.get('e.force:refreshView').fire();
          }
      });
      $A.enqueueAction(action);
  },
  getBankRule : function(component, event, helper) {		
      var action = component.get('c.GetBankRule');
      var getUrlParameter = function getUrlParameter(sParam) {
        try{
            var searchString = document.URL.split('?')[1];
            var sPageURL = decodeURIComponent(searchString),
                sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');                 
                if (sParameterName[0] === sParam)  {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        } catch(ex){}
    };
      action.setParams({ 
          "bankId" : getUrlParameter('bankId')  
      }); 
      action.setCallback(this, function(actionResult) {  
          var state = actionResult.getState(); 
          if(state === "SUCCESS"){
              var result = actionResult.getReturnValue();
              
              component.set("v.bankRule", result);
              component.set("v.bankId", result.bank.Id);
              component.set("v.ruleId", result.rule.Id);
              debugger;
              if(result.bank.Tenant_Bank_Type__c =='Party Bank' || result.bank.Tenant_Type__c =='Corporate'){
                  component.set("v.pIPApprovalRole", result.CUUsersInp); 
                  component.set("v.pValApprovalRole", result.CUUsersval); 
                  component.set("v.pApprovalRole1", result.CUUsersapp1); 
                  component.set("v.pApprovalRole2", result.CUUsersapp2); 
                  component.set("v.pRequestApprovalRole", result.CUUsersman); 
                  
                  //var cmpTarget = component.find('dropIPApprovalRole');
                  //cmpTarget.set("v.disabled", true);
                  //window.setTimeout($A.getCallback(function() { 
                  //    if(component.isValid()) {
                  $A.enqueueAction(component.get('c.changePValidationApprovalNeeded'));
                  $A.enqueueAction(component.get('c.changePApprovalsNeeded'));
                  $A.enqueueAction(component.get('c.changePApprovalLevel'));
                  $A.enqueueAction(component.get('c.changePRequestApprovalNeeded'));
                  component.find('dropIPApprovalRole').set("v.value", "");
                  component.find('dropPRequestApprovalRole').set("v.value", "");
                  debugger;
              }else{
                  component.set("v.cpValidationApprovalRole", result.CUUsersCPval); 
                  component.set("v.cpBidder", result.CUUsersCPbid); 
                  component.find('dropCPBidder').set("v.value", "");
                  component.find('dropCPValidator').set("v.value", "");}
              //    }
              //}), 10); 
              if(result.rule.Id !=undefined){
                  debugger;
                  //component.find("dropPRequestApprovalRole").set("v.disabled", true);
                  component.find("dropPValidationApprovalRole").set("v.disabled", true);
                  //component.find("dropIPApprovalRole").set("v.disabled", true);
                  component.find("dropPApprovalLevel").set("v.disabled", true);
                  component.find("dropPApprovalRole1").set("v.disabled", true);
                  component.find("dropPApprovalRole2").set("v.disabled", true);
                  component.find("pValidationApprovalNeeded").set("v.disabled", true);
                  
              }
          }
          helper.createObjectData(component, event);
      }); 
      $A.enqueueAction(action);
  },
  saveRule: function(component, event, helper) {
      var errorFlag = false;
      debugger;
     /* var fchk = component.get("v.flag");
      if(fchk !='FCP'){
          component.set("v.step1ValidatorError", '');
          component.set("v.step2LevelError", '');
          component.set("v.step2Approver1Error", '');
          component.set("v.step2Approver2Error", '');
          component.set("v.step3RequestApprovalError", '');
          
          //step 1
          var cmpSource = component.find("pValidationApprovalNeeded");
          var cmpSourceChecked = cmpSource.get("v.checked");
          
          var cmpTarget = component.find('dropPValidationApprovalRole');
          var cmpTargetValue = cmpTarget.get("v.value");
          
          if(cmpSourceChecked) {
              if(cmpTargetValue == '') {
                  errorFlag = true;
                  component.set("v.step1ValidatorError", 'Please select validator.');
              }
          }  
          
          //step 2
          cmpSource = component.find("pApprovalsNeeded");
          cmpSourceChecked = cmpSource.get("v.checked");
          
          cmpTarget = component.find('dropPApprovalLevel');
          cmpTargetValue = cmpTarget.get("v.value");
          
          var cmpTarget1 = component.find('dropPApprovalRole1'); 
          var cmpTarget1Value = cmpTarget1.get("v.value");
          
          var cmpTarget2 = component.find('dropPApprovalRole2');        
          var cmpTarget2Value = cmpTarget2.get("v.value");
          
          if(cmpSourceChecked) {
              if(cmpTargetValue == '') {
                  errorFlag = true;
                  component.set("v.step2LevelError", 'Please select level.');
              } else {
                  if(cmpTarget1Value == '') {
                      errorFlag = true;
                      component.set("v.step2Approver1Error", 'Please select approver 1.');
                  }
                  if(cmpTargetValue == '2') {
                      if(cmpTarget2Value == '') {
                          errorFlag = true;
                          component.set("v.step2Approver2Error", 'Please select approver 2.');
                      }
                  } 
              }
          }    
          
          //step 3
          cmpSource = component.find("pRequestApprovalNeeded");
          cmpSourceChecked = cmpSource.get("v.checked");
          
          cmpTarget = component.find('dropPRequestApprovalRole');
          cmpTargetValue = cmpTarget.get("v.value");
          
          if(cmpSourceChecked) {
              if(cmpTargetValue == '') {
                  errorFlag = true;
                  component.set("v.step3RequestApprovalError", 'Please select approver.');
              }
          }
      }*/
      if(!errorFlag) {
          component.find("btnSaveRule").set("v.disabled", true); 
          var bankRule = component.get("v.bankRule.rule");
          var action = component.get("c.SaveRule");
          action.setParams({
              "rule" : bankRule,
              "bnkid" : component.get("v.bankId")
          }); 
          action.setCallback(this, function(actionResult) {
              var state = actionResult.getState();
              if(state === "SUCCESS"){
                  debugger;
                  var ruleId = actionResult.getReturnValue(); 
                  component.set("v.onboard", true);
                  
              }
              component.find("btnSaveRule").set("v.disabled", false);
          });
          $A.enqueueAction(action);
      }
  },
  EdiRule: function(component, event, helper) {
      debugger;
      var errorFlag = false;
      var chkaction = component.get("c.ChkTransaction");
      chkaction.setParams({
          "bnkid" : component.get("v.bankId")
      }); 
      chkaction.setCallback(this, function(chkactionResult) {
          var chkstate = chkactionResult.getState();
          if(chkstate === "SUCCESS"){
              debugger;
              var chkresult = chkactionResult.getReturnValue(); 
              errorFlag = true;
              if(chkresult > 0)
              {
                  component.set("v.Countincomplete", chkresult);
                  component.set("v.Editerror", true);
              }else{
                  $A.get('e.force:refreshView').fire();
              }
          }
      });
      $A.enqueueAction(chkaction);
      
  }, 
  edittrasactioncount: function(component, event) {
      component.set("v.Editerror", false);        
  },
  onboard: function(component, event) {
      component.set("v.onboard", false);
      $A.get('e.force:refreshView').fire();
  },
  changeEnginerole: function(component, event, helper) {
      debugger;
      //alert('Change');
      var cmpTarget = component.find('selectEnginerrole').get("v.value");
      // alert(cmpTarget);
      if(cmpTarget =='Validator Role'){ 
          document.getElementById('divvalidator').style.display = 'block';
          document.getElementById('divapprover').style.display = 'none';
      }else if(cmpTarget =='Comment Role'){
          document.getElementById('divapprover').style.display = 'block';
          document.getElementById('divvalidator').style.display = 'none';
          
      }
      
      
  },
  changePValidationApprovalNeeded: function(component, event, helper) {  
      var cmpSource = component.find("pValidationApprovalNeeded");
      var cmpSourceChecked = cmpSource.get("v.checked");
      var cmpTarget = component.find('dropPValidationApprovalRole');
      var sv = component.find("Selectvalidator");
      if(cmpSourceChecked) {
          //cmpTarget.set("v.disabled", false);
          $A.util.removeClass(sv, "toggle");
          
          
      } else {
          cmpTarget.set("v.value", "");
          component.find('dropPValidationApprovalRole').set("v.value", "");
          //cmpTarget.set("v.disabled", true);
          $A.util.addClass(sv, "toggle");
      }
  },
  changePApprovalsNeeded: function(component, event, helper) {  
      var cmpSource = component.find("pApprovalsNeeded");
      var cmpSourceChecked = cmpSource.get("v.checked");
      var cmpTarget = component.find('dropPApprovalLevel');
      var applevllbl = component.find("applevllbl");
      var applevl = component.find("applevl");
      var app1lbl = component.find("app1lbl");
      var app1 = component.find("app1");
      var app2lbl = component.find("app2lbl");
      var app2 = component.find("app2");
      if(cmpSourceChecked) {
          //cmpTarget.set("v.disabled", false);
          $A.util.removeClass(applevllbl, "toggle");
          $A.util.removeClass(applevl, "toggle");
          
      } else {
          cmpTarget.set("v.value", "");
          component.find('pApprovalsNeeded').set("v.value", "");
          //cmpTarget.set("v.disabled", true);
          $A.util.addClass(applevllbl, "toggle");
          $A.util.addClass(applevl, "toggle");
          
      }
      var changePApprovalLevel = component.get('c.changePApprovalLevel');
      $A.enqueueAction(changePApprovalLevel);
  },
  changePApprovalLevel: function(component, event, helper) {  
      var cmpSource = component.find("dropPApprovalLevel");
      var cmpSourceValue = cmpSource.get("v.value");
      var cmpTarget1 = component.find('dropPApprovalRole1'); 
      var cmpTarget2 = component.find('dropPApprovalRole2');
      var app1lbl = component.find("app1lbl");
      var app1 = component.find("app1");  
      var app2lbl = component.find("app2lbl");
      var app2 = component.find("app2");
      
      if(cmpSourceValue == "1"){
          //cmpTarget1.set("v.disabled", false);
          cmpTarget2.set("v.value", "");
          //cmpTarget2.set("v.disabled", true);
          $A.util.removeClass(app1lbl, "toggle");
          $A.util.removeClass(app1, "toggle");
          $A.util.addClass(app2lbl, "toggle");
          $A.util.addClass(app2, "toggle");
      } else if(cmpSourceValue == "2") {
          //cmpTarget2.set("v.disabled", false);
          //cmpTarget1.set("v.disabled", false);
          $A.util.removeClass(app1lbl, "toggle");
          $A.util.removeClass(app1, "toggle");
          $A.util.removeClass(app2lbl, "toggle"); 
          $A.util.removeClass(app2, "toggle");
      } else {
          cmpTarget1.set("v.value", "");
          //cmpTarget1.set("v.disabled", true);
          cmpTarget2.set("v.value", "");
          //cmpTarget2.set("v.disabled", true);
          $A.util.addClass(app1lbl, "toggle");
          $A.util.addClass(app1, "toggle");
          $A.util.addClass(app2lbl, "toggle");
          $A.util.addClass(app2, "toggle");
          
      }
  },
  changePRequestApprovalNeeded: function(component, event, helper) {  
      var cmpSource = component.find("pRequestApprovalNeeded");
      var cmpSourceChecked = cmpSource.get("v.checked");
      /*var cmpTarget = component.find('dropPRequestApprovalRole');
        if(cmpSourceChecked) {
            cmpTarget.set("v.disabled", false);
        } else {
            cmpTarget.set("v.disabled", true);
            cmpTarget.set("v.value", "");
        }*/
    },
  // function for save the Records 
  Save: function(component, event, helper) {
      debugger;
      // first call the helper function in if block which will return true or false.
      // this helper function check the "first Name" will not be blank on each row.
      //if (helper.validateRequired(component, event)) {
      // call the apex class method for save the Contact List
      // with pass the contact List attribute to method param.  
      var childComponent =  component.find('child').get("v.ContactInstance");
      var userRole = component.find('child').get("v.userRole");
      var cmpTarget = component.get("v.contactList");
      var action = component.get("c.saveContacts");
      action.setParams({
          "ListContact": childComponent
      });
      // set call back 
      action.setCallback(this, function(response) {
          var state = response.getState();
          if (state === "SUCCESS") {
              // if response if success then reset/blank the 'contactList' Attribute 
              // and call the common helper method for create a default Object Data to Contact List 
              component.set("v.contactList", []);
              helper.createObjectData(component, event);
              // alert('record Save');
          }
      });
      // enqueue the server side action  
      $A.enqueueAction(action);
      // }
  },
  // function for create new object Row in Contact List 
  addNewRow: function(component, event, helper) {
      debugger;
      //alert('addNewRow-BankRule');
      // call the comman "createObjectData" helper method for add new Object Row to List  
      helper.createObjectData(component, event);
  },
  
  // function for delete the row 
  removeDeletedRow: function(component, event, helper) {
      // get the selected row Index for delete, from Lightning Event Attribute  
      var index = event.getParam("indexVar");
      // get the all List (contactList attribute) and remove the Object Element Using splice method    
      var AllRowsList = component.get("v.contactList");
      AllRowsList.splice(index, 1);
      // set the contactList after remove selected row element  
      component.set("v.contactList", AllRowsList);
  },
 })