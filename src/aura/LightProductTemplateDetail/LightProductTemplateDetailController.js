({
   
    doneRendering: function(component) {
        window.resize = function(){ 
            var profileMenuRegion = document.getElementsByClassName("profileMenuRegion")[0];
            var navigation = document.getElementsByClassName("navigation")[0];
            var pageHeader = document.getElementsByClassName("slds-page-header")[0];
            var pageContent = document.getElementsByClassName("slds-page-content")[0];
            
            var totalHieght = profileMenuRegion.clientHeight;
            totalHieght = totalHieght + navigation.clientHeight;
            totalHieght = totalHieght + (pageHeader.clientHeight+8);
            
            if((window.innerHeight - totalHieght) <= 144){
                pageContent.style.height = "144px";
            } else {
                pageContent.style.height = (window.innerHeight - totalHieght)+"px";
            }
        }
        window.resize();
        window.addEventListener('resize', resize, true);
         
    },
    TransNew: function(component) {
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/create-transaction';
    },
    getFinalizedDetails: function(component) {
        debugger;
        if(localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.ACP_ProfileID}") || localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.SACP_ProfileID}") || localStorage.getItem('LoggeduserProfile') ==	$A.get("{!$Label.c.BMCP_ProfileID}") || localStorage.getItem('LoggeduserProfile') ==$A.get("{!$Label.c.OBCP_ProfileID}"))
        {
            component.set("v.Adminprofile",true); 
        }
        else{
            component.set("v.Adminprofile",false);
        }
       var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Product Template Detail';
        var action = component.get('c.getProductTemplate');         
        var getUrlParameter = function getUrlParameter(sParam) {
           	try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        
        var templateId = getUrlParameter('templateId');
        action.setParams({
            "templateId" : templateId
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
           // $A.log(actionResult);
            if(state === "SUCCESS"){
                debugger;
                component.set("v.ProductTemplate", result);
                component.set("v.recoId", result.ProdTemp.Id);
                component.set("v.Fields", result.strList);
                var custs = [];
                for(var key in result.strList){
                    debugger;
                    custs.push({value:result.strList[key], key:key}); //Here we are creating the list to show on UI.
                }
                component.set("v.JsonFields",custs);
                //alert(custs);
            }
            else if(state === "ERROR") {
                debugger;
                var emptyTask = component.get("v.ProductTemplate");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.ProductTemplate", emptyTask);
            }
            var menu = document.getElementById('Product Templates');
            var classArr = menu.className.split(" ");
            if (classArr.indexOf("current") == -1) {
                menu.className += " " + "current";
            }
        }); 
        $A.enqueueAction(action);
    },  

     InactiveProdTemp : function(component, event) { 
        component.set("v.Inactive1", true);       
    },
       saveTR: function(component, event) {
        debugger;
        var jsonObj='[';
        var getAllId = component.find("boxPack");
        var len=getAllId.length-1;
        var j = 0;
        for (var i = 0; i < getAllId.length; i++) {        
            jsonObj=jsonObj+'{';
            if (getAllId[i].get("v.value") == true) {
                jsonObj=jsonObj+'"ID":"'+getAllId[i].get("v.text")+'","Flag":"TRUE"';
                j++;
            }else
            {  jsonObj=jsonObj+'"ID":"'+getAllId[i].get("v.text")+'","Flag":"FALSE"';
              
            }
            if(len==i){
                jsonObj=jsonObj+'}';
            }else{
                jsonObj=jsonObj+'},';
            }
        }  
        jsonObj=jsonObj+']';
        console.log('#json1#'+jsonObj); 
        component.set('v.maskcount',j); 
        
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        debugger;
        var action = component.get('c.SaveRFI'); 
        var Tnsid=getUrlParameter('templateId');
        action.setParams({
            "RFI" : jsonObj,
            "Transid" : Tnsid
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.showTransabox", false); 
            }
        });
        $A.enqueueAction(action);
    },
    Inactivecancel: function(component, event) {
        component.set("v.Inactive1", false);
    },
    Transactionbox: function(component, event) {
        component.set("v.showTransabox", true);
    },
    POPrequestForInfocel: function(component, event) {
        component.set("v.showTransabox", false);
    },
     inactiveCheck: function(component, event, helper) {
        if(component.find("chkInactive").get('v.value')==true)
        {
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "false");
        }
        else{
            debugger;
            component.find("Inactivebtn1").set("v.disabled", "true");
        }
    },
    
     btnInactive: function(component, event, helper) {
         debugger;
        var action = component.get('c.Inactive'); 
         var getUrlParameter = function getUrlParameter(sParam) {
           	try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        
        var templateId = getUrlParameter('templateId');
        action.setParams({
            "templateId" : templateId,
             "body5" : component.find("body5").get("v.value")
        });
     /*   action.setParams({
            "templateId" : component.get("v.recordId"),
            "body5" : component.find("body5").get("v.value")
        }); */
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            component.set("v.Inactive1", false);
            component.set("v.SecondInactive", true);
            if(state === "SUCCESS"){
                component.set("v.ProductTemplate", result);
                component.set("v.Fields", result.strList);               
            }
            else if(state === "ERROR") {
                var emptyTask = component.get("v.ProductTemplate");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.ProductTemplate", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
})