({
    
    loadinit: function (component, event, helper){
      debugger;
        if(document.URL.split('/')[6]!=undefined){
            component.set("v.showchk",true);
        }else{
                component.set("v.showchk",false);
        }
    },
    backtoreport: function (component, event, helper){
       
        window.location='https://tradeaix-stratizant.cs78.force.com/dev/s/report/Report/Recent?queryScope=mru';
        debugger;
    },
    export: function (component, event, helper){
        
        if(document.URL.split('/')[6]!=undefined){
            component.set("v.showchk",true);
            window.location='https://tradeaix-stratizant.cs78.force.com/dev/servlet/PrintableViewDownloadServlet?isdtp=p1&reportId='+document.URL.split('/')[6];
        }else{
            component.set("v.showchk",false);
        }        
        debugger;
    },
    doneRendering: function(component) {        
        window.resize = function(){  
            var profileMenuRegion = document.getElementsByClassName("profileMenuRegion")[0];
            var navigation = document.getElementsByClassName("navigation")[0];
            var pageHeader = document.getElementsByClassName("slds-page-header")[0];
            var pageContent = document.getElementsByClassName("centerRegion")[0];
            
            var totalHieght = profileMenuRegion.clientHeight;
            totalHieght = totalHieght + navigation.clientHeight;
            totalHieght = totalHieght + (pageHeader.clientHeight+8); 
            if(typeof(pageContent) !='undefined'){
                if((window.innerHeight - totalHieght) <= 144){
                    pageContent.style.height = "144px";
                } else {
                    pageContent.style.height = (window.innerHeight - totalHieght)+"px";
                }
            }
        }
        window.resize();
        window.addEventListener('resize', resize, true);
    },
    getReportDetails : function(component, event, helper) {		         
        debugger;
       //alert(reportTitle);
        component.set("v.canShowFilter", false);
        var interval = setInterval(function(){
            var reportTitle = helper.getReportTitle();
            if(reportTitle.toLowerCase() != 'detail'){
                component.set("v.title", reportTitle);
                if(reportTitle == 'Bank User Details'){
                    component.set("v.canShowFilter", true);
                }
            }
            clearInterval(interval);
        }, 500);
        
        var action = component.get('c.getReportFilterUrl'); 
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.reportFilterUrl", result);
                console.log(result);
            }
        }); 
        $A.enqueueAction(action);
        var ActionTitle = component.get("c.getloggeduserBrowserTitle");
        ActionTitle.setCallback(this, function(actionResultTitle) {
            var statetitle = actionResultTitle.getState(); 
            var resulttitle = actionResultTitle.getReturnValue();
            if(statetitle === "SUCCESS"){
                debugger;
                document.title =resulttitle +' :: Reports';
            }
        });
        $A.enqueueAction(ActionTitle);
        
    },
    updateFilter: function (component, event, helper){
        var forceFilterButton = parent.document.getElementsByClassName('forceFilterButton')[0];
        forceFilterButton.click();
    },
    exportCsv: function (component, event, helper){
        debugger;
        var action = component.get('c.getReportData');
        action.setParams({
            "reportId" : '00Om0000000abKqEAI'
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                debugger;
                var csv = result;
                /*
                if (csv == null){return;} 
                var blob = new Blob([csv]);
                if (navigator.msSaveBlob) { // IE 10+
                    navigator.msSaveBlob(blob, filename);
                } else {
                    var hiddenElement = document.createElement("a");
                    if (hiddenElement.download !== undefined) { 
                        var url = URL.createObjectURL(blob,{ type: 'text/csv;charset=utf-8;' });
                        hiddenElement.setAttribute("href", url);
                        hiddenElement.setAttribute("download", filename);
                        hiddenElement.style = "visibility:hidden";
                        document.body.appendChild(hiddenElement);
                        hiddenElement.click();
                        document.body.removeChild(hiddenElement);
                    }
                } 
                */
            }
        }); 
        $A.enqueueAction(action);
    },
})