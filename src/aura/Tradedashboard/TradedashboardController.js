({
    load: function(component) { 
        
        //var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
        document.title ='Dashboard';
        debugger;       
        var ACP = $A.get("{!$Label.c.ACP_ProfileID}");
        var SACP = $A.get("{!$Label.c.SACP_ProfileID}");  
        var AMCP = $A.get("{!$Label.c.AMCP}");
        component.set("v.sessionusers",localStorage.getItem('UserSession')); 
        var Action = component.get("c.getUsername");   
        Action.setParams({
            "username" : localStorage.getItem('UserSession')                
        });        
        Action.setCallback(this, function(actionResult) {
            debugger; 
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                var opts = [];
                opts = [ 
                    { label: "Active", value: "Active", selected: "true"},
                    { label: "Inactive", value: "Inactive" },
                    { label: "All", value: "All"},
                ];  
                    component.set("v.filterOptions", opts); 
                    debugger;
                    component.set("v.selectedItem", 'Active'); 
                  
                    if(results[0].Profiles__c== ACP)
                    {
                        component.set("v.dashcheck",'TradeAdmin');
                        component.set("v.Tenantcheck",true);
                        
                    }else if(results[0].Profiles__c== SACP || results[0].Profiles__c== AMCP){
                        
                        component.set("v.dashcheck",'Admin');
                        component.set("v.Tenantcheck",false);
                    }else{
                        component.set("v.dashcheck",'user');
                        component.set("v.Tenantcheck",false);
                    }
                var Action1 = component.get("c.loadorgs");   
                Action1.setParams({
                    "username" : localStorage.getItem('UserSession')                
                });        
                Action1.setCallback(this, function(actionResult1) {
                    debugger;
                    var state = actionResult1.getState(); 
                    var results = actionResult1.getReturnValue();
                    if(state === "SUCCESS"){
                        component.set("v.OrgFilter",results);
                    }
                    
                }); 
                $A.enqueueAction(Action1);                
                component.set("v.Tids",results[0].Tenant__c);
                component.set("v.sessionusername",results[0].First_Name__c+' '+results[0].Last_Name__c);
                component.set("v.UserIds",results[0].Id);
                var Tid = component.get("v.Tids"); 
                var action = component.get('c.getDashboardData');
                action.setParams({
                    "Tenant" :  Tid,
                    "Loggeduserid" : component.get("v.UserIds"),
                    "Tenantcheck" : component.get("v.Tenantcheck")
                }); 
                action.setCallback(this, function(actionResult) { 
                    var state = actionResult.getState(); 
                    if(state === "SUCCESS"){
                        debugger;
                        var result = actionResult.getReturnValue();
                        component.set("v.header", result.Header);
                        component.set("v.TranspastList",result.Header.TranspastList);
                        component.set("v.TranstodayList",result.Header.TranstodayList);
                        component.set("v.BankMSg",result.Header.BankMSg);
                        component.set("v.BankMsglink",result.Header.BankMsglink);
                        component.set("v.Tenant",result.Header.TenantList);
                        component.set("v.Tenantid",localStorage.getItem('LoggeduserTenantID'));
                        if(result.Header.BankMSg !=''){
                            component.set("v.corporateKeyword",result.Header.BankMSg);
                            component.set("v.editcall",'no');
                        }
                        else
                        {
                            component.set("v.editcall",'yes');
                        }
                    }
                }); 
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(Action); 
        
        
        
    },
    
    handleClick : function(component, event, helper){
        debugger;
        var Action = component.get("c.getRunasadmin");               
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.adminstring",results);
            }
        });
        $A.enqueueAction(Action); 
    },
    savecorporatetxt : function(component, event,helper) { 
        debugger; 
        var txtcorporate = component.get("v.corporateKeyword"); 
        var errorFlag = 'false';
        if(txtcorporate == '' || txtcorporate == null || txtcorporate == undefined || txtcorporate.trim().length*1 == 0) 
        {  
            errorFlag = 'true';
            component.set("v.txtcor", 'Input field required'); 
        }
        else{  
            component.set("v.txtcor", ''); 
        }  
        if(errorFlag == 'false')
        { 
            var action = component.get('c.savecorporatetxtcall'); 
            action.setParams({ 
                "txtcor" : txtcorporate,
                "LoggeduserTenantid" :localStorage.getItem("LoggeduserTenantID"),
                "LoggedUserId" : localStorage.getItem('UserSession')
            });
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    debugger;
                    $A.get('e.force:refreshView').fire();
                    
                } 
            });
            $A.enqueueAction(action);
        }
    },
    editclick: function(component){
        //component.set("v.BankMSg",'');
        component.set("v.editcall",'yes');
    },
    cancelcorporatetxt: function(component){
        debugger;
        //component.set("v.BankMSg",'');
        component.set("v.editcall",'no');
    },
    generateFunnelChart : function(component, event, helper) {
        debugger;
        if(component.get("v.selectedTenantid") ==undefined){
            var action = component.get('c.getcount');   
            action.setParams({ 
                "Tenant" : localStorage.getItem('UserSession')
            });
        }else{
            var action = component.get('c.getcountTenant');
            action.setParams({ 
                "Tenant" : component.get("v.selectedTenantid")
            });
        }
        
        //  alert("generateFunnelChart"+localStorage.getItem('UserSession'));
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                debugger;
                var result = actionResult.getReturnValue();
                var createdcount =result.createdcount;
                var Publishedcount =result.Publishedcount;
                var RFICount =result.RFICount;
                var Biddingcount =result.Biddingcount;
                var Completedcount =result.Completedcount;
                
                //var ctx = component.find("linechart").getElement();
                var chart = AmCharts.makeChart( "chartdiv", {
                    "type": "funnel",
                    "theme": "light",
                    "dataProvider": [ {
                        "title": "Created",
                        "value": createdcount
                    }, {
                        "title": "Approved",
                        "value": Publishedcount
                    },  {
                        "title": "RFI/RFQ",
                        "value": RFICount
                    },{
                        "title": "Bidding",
                        "value": Biddingcount
                    }, {
                        
                        "title": "Completed",
                        "value": Completedcount
                    }],
  "balloon": {
    "fixedPosition": true
  },
  "valueField": "value",
  "titleField": "title",
  "marginRight": 240,
  "marginLeft": 50,
  "startX": -500,
  "rotate": true,
  "labelPosition": "right",
  "balloonText": "[[title]]: [[value]]n[[description]]",
  "export": {
    "enabled": true
  }
} );
                /* var chart = AmCharts.makeChart( "chartdiv", {
                    
                    "type": "funnel",
                    "theme": "light",
                    "dataProvider": [ {
                        "title": "Created",
                        "value": createdcount
                    }, {
                        "title": "Approved",
                        "value": Publishedcount
                    }, {
                        "title": "RFI/RFQ",
                        "value": RFICount
                    }, {
                        "title": "Bidding",
                        "value": Biddingcount
                    }, {
                        "title": "Completed",
                        "value": Completedcount
                    }],  
                    "balloon": {
                        "fixedPosition": true
                    },
                    "valueField": "value",
                    "titleField": "title",
                    "marginRight": 240,
                    "marginLeft": 50,
                    "startX": -500,
                    "depth3D": 100,
                    "angle": 40,
                    "outlineAlpha": 1,
                    "outlineColor": "#FFFFFF",
                    "outlineThickness": 2,
                    "labelPosition": "right",
                    "balloonText": "[[title]]: [[value]][[description]]",
                    "export": {
                        "enabled": true
                    }
                } );*/
                
            }
        });
        $A.enqueueAction(action); 
    },
    generatepieChart : function(component, event, helper) {
        debugger;
                
        var action = component.get('c.getDashboardData');
        action.setParams({
            "Tenant" :  localStorage.getItem('LoggeduserTenantID'),
            "Loggeduserid" : component.get("v.UserIds"),
            "Tenantcheck" : false
        }); 
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                debugger;
                var result = actionResult.getReturnValue();
                var chart = AmCharts.makeChart("chartdivManager",{
                    "type"    : "pie",
                    "titleField"  : "category",
                    "valueField"  : "column-1",
                    "dataProvider"  : [
                        {
                            "category": "Transaction Approved",
                            "column-1": result.Header.TACount
                        },
                        {
                            "category": "Requested For Info",
                            "column-1": result.Header.TRFICount
                        },
                        {
                            "category": "Requested For Quote",
                            "column-1": result.Header.TRFQCount
                        },
                        {
                            "category": "Completed Transaction",
                            "column-1": result.Header.TCCount
                        }
                    ]
                });

            }
        });
        $A.enqueueAction(action); 
    },
    onSingleSelectChange: function(component, event, helper) {
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value");         
        if(selectCmp =='All Realms'){
            var tchk = true;
            selectCmp = component.get("v.Tids");              
        }else{
            var tchk = false;
        }
        component.set("v.selectedTenantid",selectCmp);         
        var action = component.get('c.getDashboardData');
        action.setParams({
            "Tenant" :  selectCmp,
            "Loggeduserid" : component.get("v.UserIds"),
            "Tenantcheck" : tchk
        }); 
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                debugger;
                var result = actionResult.getReturnValue();
                component.set("v.header", result.Header);
                component.set("v.TranspastList",result.Header.TranspastList);
                component.set("v.TranstodayList",result.Header.TranstodayList);
                //component.set("v.Tenant",result.Header.TenantList);
                
                var a = component.get('c.generateFunnelChart');
                $A.enqueueAction(a);
            }
        }); 
        $A.enqueueAction(action);
        
    },
    
})