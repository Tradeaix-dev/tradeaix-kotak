({
    
    qsToEventMap: {
        'startURL'  : 'e.c:setStartUrl'
    }, 
    
    handleLogin: function (component, event, helpler) {
        debugger;
        var username = component.find("username").get("v.value");
        var password = component.find("password").get("v.value");
        var action = component.get("c.login"); 
        /******browser check-START******/
        var browserType = navigator.sayswho= (function(){
            var ua= navigator.userAgent, tem,
                M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE '+(tem[1] || '');
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        })();
        //alert(browserType);
        //alert(component.get("v.ipAddress"));
        /******browser check-END******/     
        var TenantID= document.URL.split('/')[5]
        
        action.setParams({username:username, password:password, TenantId:TenantID, browsername:browserType ,ipAddress:component.get("v.ipAddress"),LoginURL:document.URL,RetURL:null});
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue =='Failed') {
                component.set("v.errorMessage",'Your login attempt has failed. Make sure the username and password are correct.');
                component.set("v.showError",true);
            }
            else
            {
                localStorage.setItem("UserSession", rtnValue);
               /* if(localStorage.getItem("Tid") !='undefined')
                {
                    alert(localStorage.getItem("Tid"));
                    alert(localStorage.getItem("processed"));                    
                    window.location = '/tradeaix/s/transactionsummary?Tids='+localStorage.getItem("Tid")+'&processed='+localStorage.getItem("processed");
                }
                else{
                    window.location = '/tradeaix/s/tradedashboard';
                }*/
                
            }
        });
        $A.enqueueAction(action);
    },
    
    getIsUsernamePasswordEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsUsernamePasswordEnabled");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isUsernamePasswordEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getIsSelfRegistrationEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsSelfRegistrationEnabled");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isSelfRegistrationEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunityForgotPasswordUrl : function (component, event, helpler) {
        var action = component.get("c.getForgotPasswordUrl");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communityForgotPasswordUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunitySelfRegisterUrl : function (component, event, helpler) {
        var action = component.get("c.getSelfRegistrationUrl");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communitySelfRegisterUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    }
})