({
    getUserList : function(component, event, helper) {
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };        
        component.set("v.Redirectchk",getUrlParameter('F'));
        var userId = getUrlParameter('userid');       
        var Action = component.get("c.getUserprofile");
        
        component.set("v.userid",localStorage.getItem("UserSession"));
        component.set("v.edituserId",userId);
        var sessionname = localStorage.getItem("UserSession");        
        if(userId == undefined){            
            component.set("v.createUser", false);
            Action.setParams({
                "username" : sessionname
            });
        }else{            
            component.set("v.createUser", true);
            Action.setParams({
                "username" : userId
            });
        }
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                if(component.get("v.createUser")){
                    component.set("v.TenantUser",results.Userlists);
                    
                }    
                component.set("v.countrycode", results.countrycodelist);
                component.set("v.Users",results.userResults);
                component.set("v.tradeaixAdmin", results.tradeaixAdmin);
                //component.set("v.Notification", results.Userlists.Enable_Notification__c);
                //component.set("v.enableEmail", results.Userlists.Enable_Email__c);
                //component.set("v.superAdmin",results.isAdmin);
                component.set("v.userCreate", results);
            }
        });
        $A.enqueueAction(Action);
    },
    
   UpdateUserprofile : function(component, event, helper) {
        debugger;     
        var ErrorFlag = 'False';
        component.set("v.validation", false);
        var firstname = component.find("firstnameVal").get("v.value");
        var lastname = component.find("lastnameVal").get("v.value");
        //var phone = component.find("phoneVal").get("v.value"); 
        var username = component.find("usernameval").get("v.value"); 
        var useremail = component.find("useremailval").get("v.value");         
        //var password = component.find("passwordval").get("v.value"); 
        var organizationValue = component.get("v.selectedOrgValue");
        var tenantValue = component.get("v.selectedTenantValue");
        var roleValue = component.find("roles").get("v.value");  
        var onlyletter =/^[A-Za-z ]+$/;
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(firstname == undefined || firstname == ""){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.firstnameError",'Input field required');
        }else if(!$A.util.isEmpty(firstname)){
            if(!firstname.match(onlyletter)){
                ErrorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.firstnameError",'Name not allowed special characters');
            }else{
                component.set("v.firstnameError",'');
            }
        }
        else{
            component.set("v.firstnameError",'');
        }
        if(lastname == undefined || lastname == ""){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.lastnameError",'Input field required');
        }else if(!$A.util.isEmpty(lastname)){
            if(!lastname.match(onlyletter)){
                ErrorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.lastnameError",'Name not allowed special characters');
            }else{
                component.set("v.lastnameError",'');
            }
        }
        else{
            component.set("v.lastnameError",'');
        }       
        if(username == undefined || username == "" ){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.usernameError",'Input field required');
        }else if(!$A.util.isEmpty(username)){
            if(!username.match(regExpEmailformat)){
                 ErrorFlag ='true';
            	 component.set("v.validation", true);
                 component.set("v.usernameError",'User Name should be email format');
            }else{
                 component.set("v.usernameError",'');
            }
            
        }
        else{
            component.set("v.usernameError",'');
        }
        
        if(useremail == undefined || useremail == ""){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.useremailError",'Input field required');
        }else if(!useremail.match(regExpEmailformat)){
            ErrorFlag = 'true'; 
            component.set("v.validation", true);
            component.set("v.useremailError", 'Please Enter a Valid Email Address'); 
        }
            else{
                component.set("v.useremailError",'');
            }
        if(organizationValue == '0'){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.organizationError",'Input field required');
        }
        else{
            component.set("v.organizationError",'');
        }
        if(tenantValue == '0'){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.tenantError",'Input field required');
        }
        else{     
            component.set("v.tenantError",'');
        }
        if(roleValue == '--Select--'){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.roleerror",'Input field required');
        }
       else{
           component.set("v.roleerror",''); 
       }   
       
       if(ErrorFlag =='False'){                    
           var Action1 = component.get("c.saveprofile");
           var sessionname = localStorage.getItem("UserSession");
           Action1.setParams({
               "username" : sessionname ,
               "tenant" : component.get("v.TenantUser"),
               "orgId" : component.get("v.selectedOrgValue"),
               "tenantId" :component.get("v.selectedTenantValue")
           });
           Action1.setCallback(this, function(actionResult1) {
               debugger;
               var state = actionResult1.getState(); 
               var results = actionResult1.getReturnValue();
               if(state === "SUCCESS"){   
                   var site = $A.get("{!$Label.c.Org_URL}");
                   if(component.get("v.edituserId")==undefined)
                   {
                       window.location = '/'+site+'/s/profileedit?userid='+results;                        
                   }else{
                       window.location = '/'+site+'/s/profileedit?userid='+component.get("v.edituserId");
                   }
                   
               }
           });
           $A.enqueueAction(Action1);
       }
   },
    
    saveUserprofile : function(component, event, helper) {
        debugger;     
        var ErrorFlag = 'False';
        component.set("v.validation", false);
        var firstname = component.find("firstnameVal").get("v.value");
        var lastname = component.find("lastnameVal").get("v.value");
        //var phone = component.find("phoneVal").get("v.value"); 
        var username = component.find("usernameval").get("v.value"); 
        var useremail = component.find("useremailval").get("v.value");         
        //var password = component.find("passwordval").get("v.value"); 
        var organizationValue = component.get("v.selectedOrgValue");
        var tenantValue = component.get("v.selectedTenantValue");
        var roleValue = component.find("roles").get("v.value");  
        var onlyletter =/^[A-Za-z ]+$/;
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(firstname == undefined || firstname == ""){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.firstnameError",'Input field required');
        }else if(!$A.util.isEmpty(firstname)){
            if(!firstname.match(onlyletter)){
                ErrorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.firstnameError",'Name not allowed special characters');
            }else{
                component.set("v.firstnameError",'');
            }
        }
        else{
            component.set("v.firstnameError",'');
        }
        if(lastname == undefined || lastname == ""){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.lastnameError",'Input field required');
        }else if(!$A.util.isEmpty(lastname)){
            if(!lastname.match(onlyletter)){
                ErrorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.lastnameError",'Name not allowed special characters');
            }else{
                component.set("v.lastnameError",'');
            }
        }
        else{
            component.set("v.lastnameError",'');
        }       
        if(username == undefined || username == "" ){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.usernameError",'Input field required');
        }else if(!$A.util.isEmpty(username)){
            if(!username.match(regExpEmailformat)){
                 ErrorFlag ='true';
            	 component.set("v.validation", true);
                 component.set("v.usernameError",'User Name should be email format');
            }else{
                 component.set("v.usernameError",'');
            }
            
        }
        else{
            component.set("v.usernameError",'');
        }
        
        if(useremail == undefined || useremail == ""){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.useremailError",'Input field required');
        }else if(!useremail.match(regExpEmailformat)){
            ErrorFlag = 'true'; 
            component.set("v.validation", true);
            component.set("v.useremailError", 'Please Enter a Valid Email Address'); 
        }
            else{
                component.set("v.useremailError",'');
            }
        if(organizationValue == '0'){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.organizationError",'Input field required');
        }
        else{
            component.set("v.organizationError",'');
        }
        if(tenantValue == '0'){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.tenantError",'Input field required');
        }
        else{     
            component.set("v.tenantError",'');
        }
        if(roleValue == '--Select--'){
            ErrorFlag ='true';
            component.set("v.validation", true);
            component.set("v.roleerror",'Input field required');
        }
        else{
            component.set("v.roleerror",''); 
        }   
        if(username != undefined || username != ""){
            var action = component.get("c.CheckTenantUserName");
            action.setParams({ "UserName": username });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var bank = response.getReturnValue(); 
                if(bank != '' && bank != undefined) {
                    if(bank == 'Already Used') { 
                        ErrorFlag = 'true';
                        component.set("v.validation", true); 
                        component.set("v.usernameError", 'This User Name already Exist. Please enter a new User Name.');
                    } else {
                        component.set("v.usernameError", ''); 
                    }
                } 
                if(ErrorFlag =='False'){                    
                    var Action1 = component.get("c.saveprofile");
                    var sessionname = localStorage.getItem("UserSession");
                    Action1.setParams({
                        "username" : sessionname ,
                        "tenant" : component.get("v.TenantUser"),
                        "orgId" : component.get("v.selectedOrgValue"),
                        "tenantId" :component.get("v.selectedTenantValue")
                    });
                    Action1.setCallback(this, function(actionResult1) {
                        debugger;
                        var state = actionResult1.getState(); 
                        var results = actionResult1.getReturnValue();
                        if(state === "SUCCESS"){   
                            var site = $A.get("{!$Label.c.Org_URL}");
                            if(component.get("v.edituserId")==undefined)
                            {
                                window.location = '/'+site+'/s/profileedit?userid='+results;                        
                            }else{
                                window.location = '/'+site+'/s/profileedit?userid='+component.get("v.edituserId");
                            }
                            
                        }
                    });
                    $A.enqueueAction(Action1);
                }
            });
            $A.enqueueAction(action);
        } 
        
    },
    cancelEdit : function(component, event, helper) {
        debugger;  
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/profileedit?userid='+component.get("v.edituserId");
    },
    cancelEditredirect : function(component, event, helper) {
        debugger;  
        var site = $A.get("{!$Label.c.Org_URL}");
        if(component.get("v.Redirectchk") =='A')
        {
           window.location = '/'+site+'/s/administration'; 
        }else{
            window.location = '/'+site+'/s/profileedit?userid='+component.get("v.edituserId");
        }
        
        
    },
    onChangeOrganization : function(component, event, helper) {
        debugger;
        var orgName = component.get("v.selectedOrgValue");
        var Action = component.get("c.getTenantlists");
        Action.setParams({
            "orgname" : orgName
        }); 
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){                      
                component.set("v.userCreate.tenantLists",results.tenantLists);
            }
        });
        $A.enqueueAction(Action);
    }
})