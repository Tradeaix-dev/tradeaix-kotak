({
    getTemplateList: function(component, event, helper) { 
        debugger;
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Product Templates';
        if(localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.ACP_ProfileID}"))
        {
            component.set("v.Adminprofile",true);
            component.set("v.Tenantcheck",true);
            var Action1 = component.get("c.loadorgs");   
            Action1.setParams({
                "username" : localStorage.getItem('UserSession')                
            });        
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.OrgFilter",results);
                }
                
            }); 
            $A.enqueueAction(Action1);
        }
        else{
            component.set("v.Tenantcheck",false);
            component.set("v.Adminprofile",false);
        }
        var action = component.get('c.getProductTemplateList');
        var self = this; 
        
        component.set("v.Tenantid",localStorage.getItem('LoggeduserTenantID'));
        var sessionid = localStorage.getItem("UserSession");       
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        }); 
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        }); 
        $A.enqueueAction(action);   
        
    },
    

    onSingleSelectChange: function(component, event, helper) {
        debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value");
        	component.set("v.selectedFilterItem",selectCmp);
            var action = component.get('c.Filter');         
            var selected = event.getSource().get("v.text");        
            component.set("v.selectedItem", selected)
            var self = this; 
            action.setParams({
                "counter" : (component.get("v.counter")).toString(),
                "sortbyField" : component.get("v.sortbyField"),
                "sortDirection" : component.get("v.sortDirection"),
                "selected" : component.get("v.selectedItem"),
                "userid" : localStorage.getItem("UserSession"),
                "Filtervalue" : selectCmp                
            });
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){  
                    helper.setGridOption(component, actionResult.getReturnValue());
                }
            }); 
            $A.enqueueAction(action); 
        }, 
    View: function(component, event, helper) {
        debugger;
        var action = component.get('c.Views'); 
        
        var selected = event.getSource().get("v.text");
        
        component.set("v.selectedItem", selected)
        var self = this; 
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem") 
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        }); 
        $A.enqueueAction(action); 
    },          
    Beginning: function(component, event, helper) {
        var action = component.get('c.getBeginning'); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    Previous: function(component, event, helper) {
        debugger;
        
        var action = component.get('c.getPrevious'); 
        var list_size = component.get('v.list_size');
        
        var counter = component.get('v.counter');
        var res = counter - list_size;  
        if(res < 0) { res = 0; }
        action.setParams({ 
            "selected" : component.get("v.selectedItem"), 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem")
            
        });
        
        action.setCallback(this, function(actionResult) { 
            
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                
                Helper.setGridOption(component, actionResult.getReturnValue());
                
            }
        });
        $A.enqueueAction(action);
    },
    Next: function(component, event, helper) {
        var action = component.get('c.getNext'); 
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = list_size + counter;
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    End: function(component, event, helper) {
        var action = component.get('c.getEnd');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var total_size = component.get('v.total_size');
        var res = total_size - (total_size % list_size); 
        if(res == total_size) { res -= list_size; }
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    sort: function(component, event, helper) {
        var action = component.get('c.SortTable'); 
        var sortfield = event.currentTarget.getAttribute("data-recId");  
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : sortfield,
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageSizes: function(component, event, helper) {
        var action = component.get('c.changelist_size');  
        var list_size = component.find("recordSize").get("v.value"); 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"), 
            "list_size" : list_size,
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageOptions: function(component, event, helper) {
        var action = component.get('c.changePage'); 
        var showpage = component.find("pageOptions").get("v.value"); 
        var list_size = component.find("recordSize").get("v.value"); 
        var counter = (parseInt(showpage)-1)*list_size;
        action.setParams({
            "counter" : counter.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "selected" : component.get("v.selectedItem"),
            "userid" : localStorage.getItem("UserSession"),
            "Filtervalue" : component.get("v.selectedFilterItem") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                helper.setGridOption(component, actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }, 
    handleChange: function (component, event, helper) {
        var selectedFields = event.getParam("value");
        var targetName = event.getSource().get("v.name");
        if(targetName == 'srcFields'){ 
            component.set("v.template.SelectedFields", selectedFields);
        }
    },
    createTemplate: function(component, event, helper) { 
        var action = component.get('c.getProductTemplate');
        
        action.setParams({
            "templateId" : "",
            "productType" : "Funded",
            "marketType" : "false",
            "templateName": ""
        }); 
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                var result = actionResult.getReturnValue();
                component.set("v.template", result);
                component.set("v.showCreate", true);
            }
            else if(state === "ERROR") {
                component.set("v.template", component.get("v.template"));
            }
        }); 
        $A.enqueueAction(action);      
    },
    cancel: function(component, event, helper) {
        component.set("v.showCreate", false);
        component.set("v.validation", false);
        component.set("v.nameError", '');
    },
    saveTemplate: function(component, event, helper) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);  
        var template = component.get("v.template");
        if(template.TemplateName == '' || template.TemplateName == null || template.TemplateName == undefined || template.TemplateName.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        } else { 
            if(helper.checkSpecialCharecter(template.TemplateName)) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.nameError", 'Please enter without special characters.'); 
            } else {  
                component.set("v.nameError", ''); 
            } 
        }
        
        if(errorFlag == 'false') { 
            debugger;
            var img = component.find("pdfImgloading");
            
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  
            
            component.find("btnSaveTemplate").set("v.disabled", true); 
            component.find("btnCancleTemplate").set("v.disabled", true); 
            var action = component.get('c.saveProductTemplate');
            action.setParams({ "template": JSON.stringify(template) });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){  
                    
                    if(result != 'Already Used' && result != '')
                    {
                        /*var action1 = component.get('c.emailProductTemplate'); 
                        action1.setParams({
                            "templateId" : result  
                        });
                        action1.setCallback(this, function(actionResult1) {
                            var state1 = actionResult1.getState();
                        });
                        $A.enqueueAction(action1); 
                        */
                        
                        component.find("btnSaveTemplate").set("v.disabled", false); 
                        component.find("btnCancleTemplate").set("v.disabled", false); 
                        
                        component.set("v.validation", false);
                        component.set("v.showCreate", false); 
                        $A.get('e.force:refreshView').fire();
                    }
                    else if(result == 'Already Used')
                    {
                        component.set("v.validation", true);
                        component.set("v.nameError", 'This Template Name has already been used. Please enter a new Template Name.');
                        component.find("btnSaveTemplate").set("v.disabled", false);
                        component.find("btnCancleTemplate").set("v.disabled", false);   
                    } 
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                }
                else if(state === "ERROR") {
                    component.find("btnSaveTemplate").set("v.disabled", false);
                    component.find("btnCancleTemplate").set("v.disabled", false); 
                    component.set("v.template", component.get("v.template"));
                    component.set("v.validation", false);
                    component.set("v.showCreate", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Failure : ',
                            'message': 'Unable to Created Template',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    handleProductTypeChange: function(component, event, helper) {
        debugger;
        var templateName = component.find("txtTemplateName").get("v.value");
        var productType = component.find("dropProductType").get("v.value"); 
        var isPrimary = component.get("v.template.IsPrimary"); 
        var action = component.get('c.getProductTemplate');
        action.setParams({
            "templateId" : "",
            "productType": productType,
            "marketType": isPrimary.toString(),
            "templateName": templateName
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                debugger;
                var result = actionResult.getReturnValue();
                component.set("v.template", result);
            }
        });
        $A.enqueueAction(action);
    },
    handleMarkerTypeChange: function(component, event, helper) {
        var market = event.getSource().get("v.label");
        if(market == 'Primary'){
            component.set("v.template.IsPrimary", true);
        } else {
            component.set("v.template.IsPrimary", false);
        }
        debugger;
        var templateName = component.find("txtTemplateName").get("v.value");
        var productType = component.find("dropProductType").get("v.value"); 
        var isPrimary = component.get("v.template.IsPrimary"); 
        var action = component.get('c.getProductTemplate');
        action.setParams({
            "templateId" : "",
            "productType": productType,
            "marketType": isPrimary.toString(),
            "templateName": templateName
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){  
                debugger;
                var result = actionResult.getReturnValue();
                component.set("v.template", result);
            }
        });
        $A.enqueueAction(action);
    }, 
})