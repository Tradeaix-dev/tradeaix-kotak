({
    getNotification : function(component, event, helper) {
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
        document.title =sitename +' : Notifications';
        var ACP = $A.get("{!$Label.c.ACP_ProfileID}");
        var AMCP = $A.get("{!$Label.c.AMCP}");
        if(localStorage.getItem("LoggeduserProfile") == AMCP){
            component.set("v.selleradmin", true);
        }else{
            component.set("v.selleradmin", false);
        }
        if(localStorage.getItem("LoggeduserProfile") == ACP){
            component.set("v.tradeamin", true);
        }else{
            component.set("v.tradeamin", false);
        }
        debugger;
        var Action = component.get("c.ShowTable");
        Action.setParams({
            "Days" :'Last 7 Days',
            "LoggedUserId" : localStorage.getItem("IDTenant")                   
        });
        Action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Notification", result);
                component.set("v.total_size",result.total_size);
            }
        });
        $A.enqueueAction(Action);
        
        
    },
    onSingleSelectChange: function(component, event, helper) {
        debugger;
         var selectCmp = component.find("InputSelectSingle").get("v.value");
         var Action = component.get("c.ShowTable");
         Action.setParams({
            "Days" :selectCmp,
             "LoggedUserId" : localStorage.getItem("IDTenant")
        });
                Action.setCallback(this, function(actionResult) {
                    var state = actionResult.getState(); 
                    var result = actionResult.getReturnValue();
                    if(state === "SUCCESS"){
                        component.set("v.Notification", result);
                        component.set("v.total_size",result.total_size);
                        //$A.get('e.force:refreshView').fire();
                    }
                });
                $A.enqueueAction(Action);
         
	 },
    OnclickNotification: function (component, event, helper) {
        var action = component.get("c.ReadNotification"); 
        action.setParams({
            "Novid" : event.target.id                    
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                
            }});
        $A.enqueueAction(action);
    },
    OnclickNotification1: function (component, event, helper) {
        var action = component.get("c.ReadNotification1"); 
        action.setParams({
            "Novid" : event.target.id                    
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
            }});
        $A.enqueueAction(action);
    },
    
                
})