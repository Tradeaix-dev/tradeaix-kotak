{
    setGridOption:function(component, result) {  
        debugger;
        component.set("v.offer", result.results);
        component.set("v.counter", result.counter); 
        component.set("v.total_size", result.total_size); 
        component.set("v.total_page", result.total_page);      
        var pageOptions=[]; 
        for(var i=0;i<=result.total_page-1;i++) { 
            pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
        } 
        var inputsel = component.find("pageOptions");
        inputsel.set("v.options", pageOptions); 
        component.set("v.list_size", result.list_size); 
        var showpage = (result.counter == 0) ? '1' : ((result.counter/result.list_size)+1).toString();
        component.set("v.showpage", showpage); 
        inputsel.set("v.value", showpage.toString());
        component.find("recordSize").set("v.value", (result.list_size).toString());  
        if (result.counter > 0) {
            component.find("disableBeginning").set("v.disabled", false);
            component.find("disablePrevious").set("v.disabled", false);
        } else {
            component.find("disableBeginning").set("v.disabled", true);
            component.find("disablePrevious").set("v.disabled", true);
        } 
        
        if (result.counter + result.list_size < result.total_size) 
        {
            component.find("disableNext").set("v.disabled", false);
            component.find("disableEnd").set("v.disabled", false); 
        } else  {
            component.find("disableNext").set("v.disabled", true);
            component.find("disableEnd").set("v.disabled", true);
        } 
    },

})