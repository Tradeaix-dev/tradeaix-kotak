({
    doneRendering: function(component) {
        /* jaya */
        window.resize = function(){
            var profileMenuRegion = document.getElementsByClassName("profileMenuRegion")[0];
            var navigation = document.getElementsByClassName("navigation")[0];
            var newHeader = document.getElementsByClassName("newHeader")[0];
            var pageHeader = document.getElementsByClassName("slds-page-header")[0];
            var gridtitle = document.getElementsByClassName("grid-title")[0];
            var tableresponsive = document.getElementsByClassName("table-responsive")[0];
            
            var pager = document.getElementsByClassName("pager-div")[0];
            
            var tblCustomizedstocktable =  document.getElementsByClassName("Customizedstocktable")[0];
            var tBody = tblCustomizedstocktable.tBodies[0];
            var tHead = tblCustomizedstocktable.tHead;
            
            var totalHieght = profileMenuRegion.clientHeight;
            totalHieght = totalHieght + navigation.clientHeight;
            totalHieght = totalHieght + newHeader.clientHeight;
            totalHieght = totalHieght + pageHeader.clientHeight;
            totalHieght = totalHieght + (tHead.clientHeight+18);
            totalHieght = totalHieght + pager.clientHeight;

            if((window.innerHeight - totalHieght) <= 124){
                tBody.style.height = "124px";
            } else {
	            tBody.style.height = (window.innerHeight - totalHieght)+"px";
            }
            tableresponsive.style.height = ((tblCustomizedstocktable.clientHeight - 35) + 14) + "px";
        }
        window.resize();
        window.addEventListener('resize', resize, true);
        /* jaya */
    },
	doInit : function(component, event, helper) {
        debugger;
		var action = component.get("c.getUserDetails");
        var self = this; 
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection") 
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);
                component.set("v.showpage", '1');
                inputsel.set("v.value", '1');
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                } 
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        }); 
		$A.enqueueAction(action);
	},
    Beginning: function(component) {
        var action = component.get('c.getBeginning'); 
        var self = this;
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection") 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);
                
                var showpage = '';
                if(result.counter == 0) 
                    showpage ='1'; 
                else
                    showpage = ((result.counter/result.list_size)+1).toString();
                
                component.set("v.showpage", showpage);
                inputsel.set("v.value", showpage);  
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                }
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    Previous: function(component) {
        var action = component.get('c.getPrevious');  
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = counter - list_size;  
        if(res < 0) 
            res = 0; 
        var self = this; 
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection")  
        });
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);
                
                var showpage = '';
                if(result.counter == 0) 
                    showpage ='1'; 
                else
                    showpage = ((result.counter/result.list_size)+1).toString();
                
                component.set("v.showpage", showpage);
                inputsel.set("v.value", showpage);  
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                }
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    Next: function(component) {
        var action = component.get('c.getNext'); 
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var res = list_size + counter;
        var self = this; 
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection")  
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);

                var showpage = '';
                if(result.counter == 0) 
                    showpage ='1'; 
                else
                    showpage = ((result.counter/result.list_size)+1).toString();
                
                component.set("v.showpage", showpage);
                inputsel.set("v.value", showpage);  
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                }
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    End: function(component) {
        var action = component.get('c.getEnd');  
        // Set up the callback
        var self = this;         
        var list_size = component.get('v.list_size');
        var counter = component.get('v.counter');
        var total_size = component.get('v.total_size');
        var res = total_size - (total_size % list_size); 
        
        if(res == total_size)
            res -= list_size; 
        
        action.setParams({ 
            "counter" : res.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection")  
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);
                
                var showpage = '';
                if(result.counter == 0) 
                    showpage ='1'; 
                else
                    showpage = ((result.counter/result.list_size)+1).toString();
                
                component.set("v.showpage", showpage);
                inputsel.set("v.value", showpage);  
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                }
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    sort:function(component, event, helper) {
        debugger;
        var action = component.get('c.SortTable'); 
        var sortfield = event.currentTarget.getAttribute("data-recId");  
        var self = this; 
        action.setParams({
            "counter" : (component.get("v.counter")).toString(),
            "sortbyField" : sortfield,
            "sortDirection" : component.get("v.sortDirection")  
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);
                
                var showpage = '';
                if(result.counter == 0) 
                    showpage ='1'; 
                else
                    showpage = ((result.counter/result.list_size)+1).toString();
                
                component.set("v.showpage", showpage);
                inputsel.set("v.value", showpage);  
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                }
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    }, 
    changePageSizes:function(component, event, helper) {
        debugger;
        var action = component.get('c.changelist_size');  
        var list_size = component.find("recordSize").get("v.value"); 
        var self = this; 
        action.setParams({
            "counter" : '0',
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection"),
            "list_size" : list_size 
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);

                var showpage = '';
                if(result.counter == 0) 
                    showpage ='1'; 
                else
                    showpage = ((result.counter/result.list_size)+1).toString();
                
                component.set("v.showpage", showpage);
                inputsel.set("v.value", showpage);  
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                }
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },  
    changePageOptions:function(component, event, helper) {
        debugger;
        var action = component.get('c.changePage'); 
        var showpage = component.find("pageOptions").get("v.value"); 
        var list_size = component.find("recordSize").get("v.value"); 
        var counter = (parseInt(showpage)-1)*list_size;
        var self = this; 
        action.setParams({
            "counter" : counter.toString(),
            "sortbyField" : component.get("v.sortbyField"),
            "sortDirection" : component.get("v.sortDirection")  
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.lstUsers", result.results);
                component.set("v.counter", result.counter);                
                component.set("v.total_size", result.total_size); 
                component.set("v.total_page", result.total_page);  
                component.set("v.sortbyField", result.sortbyField);
                component.set("v.sortDirection", result.sortDirection); 
                component.set("v.MarketingPageUrl", result.MarketingPageUrl);
                component.set("v.ContactUsMail", result.ContactUsMail);
                
	            var pageOptions = []; 
                for(var i=0;i<=result.total_page-1;i++) { 
                    pageOptions.push({"class": "optionClass", label: i+1, value: i+1});
                } 
                
                var inputsel = component.find("pageOptions");
                inputsel.set("v.options", pageOptions); 
                component.set("v.allCompletedSalesList", result.allCompletedSalesList); 
                component.set("v.list_size", result.list_size);
                
                var showpage = '';
                if(result.counter == 0) 
                    showpage ='1'; 
                else
                    showpage = ((result.counter/result.list_size)+1).toString();
                
                component.set("v.showpage", showpage);
                inputsel.set("v.value", showpage);  
                component.find("recordSize").set("v.value", (result.list_size).toString());
                
                if (result.counter > 0) {
                   component.find("disableBeginning").set("v.disabled", false);
                   component.find("disablePrevious").set("v.disabled", false);
                } else {
                   component.find("disableBeginning").set("v.disabled", true);
                   component.find("disablePrevious").set("v.disabled", true);
                } 
                
                if (result.counter + result.list_size < result.total_size) {
                   component.find("disableNext").set("v.disabled", false);
                   component.find("disableEnd").set("v.disabled", false); 
                } else {
                   component.find("disableNext").set("v.disabled", true);
                   component.find("disableEnd").set("v.disabled", true);
                }
            } else if(state === "ERROR") {
                var emptyTask = component.get("v.Offer");
                emptyTask.Subject = "";
                emptyTask.Description = "";  
                component.set("v.Offer", emptyTask);
            }
        });
        $A.enqueueAction(action);
    },
    
    /*  Create and remove button logic  */
    crtUser : function(component, event) {
        component.set("v.createUser", true);
    },
	crtUserCancel : function(component, event) {
        component.set("v.createUser", false);
    },
    rmvUser : function(component, event) {
        component.set("v.removeUser", true);
    },
    rmvUserCancel : function(component, event) { 
        component.set("v.removeUser", false);
    },
    
})