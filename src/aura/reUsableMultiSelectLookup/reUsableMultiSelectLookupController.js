({
    doinit :  function(component,event,helper){
        debugger; 
        component.set("v.lstSelectedRecords", []);
    },
    onblur : function(component,event,helper){
        debugger;
        // on mouse leave clear the listOfSeachRecords & hide the search result component 
        component.set("v.listOfSearchRecords", null );
        component.set("v.SearchKeyWord", '');
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    onfocus : function(component,event,helper){        
        // show the spinner,show child search result component and call helper function
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        component.set("v.listOfSearchRecords", null ); 
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC 
        var getInputkeyWord = '';
        helper.searchHelper(component,event,getInputkeyWord);
    },
    
    keyPressController : function(component, event, helper) {
        debugger;
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        // get the search Input keyword   
        var getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if(getInputkeyWord.length > 0){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    
    // function for clear the Record Selaction 
    clear :function(component,event,heplper){
        debugger;
        var selectedPillId = event.getSource().get("v.name");
        var AllPillsList = component.get("v.lstSelectedRecords"); 
        
        for(var i = 0; i < AllPillsList.length; i++){
            if(AllPillsList[i].Id == selectedPillId){
                AllPillsList.splice(i, 1);
                component.set("v.lstSelectedRecords", AllPillsList);
            }  
        }
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );      
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        debugger;        
        component.set("v.SearchKeyWord",null);        
        // get the selected object record from the COMPONENT event 	 
        var listSelectedItems =  component.get("v.lstSelectedRecords");
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedAccountGetFromEvent);
        component.set("v.lstSelectedRecords" , listSelectedItems); 
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open'); 
    },
    saveshareusers : function(component, event, helper) {
        debugger;
        var listSelectedItems = component.get("v.lstSelectedRecords");
        if(listSelectedItems =='')
        {
            component.set("v.outsidepeople",true);
        }else{
            component.set("v.outsidepeople",false);
        
        
        var action = component.get('c.foldersharing');
        //alert(component.get("v.DocumentId"));        
        action.setParams({    
            "fileId" : component.get("v.DocumentId"),
            "userlist" : listSelectedItems,
            "LoggedUserId" : localStorage.getItem("IDTenant")            
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(results === "Success"){  
                component.set("v.lstSelectedRecords", []);
                var getSelectRecord = component.get("v.oRecordUser");
                var compEvent = component.getEvent("oSelectedRecord");               
                // set the Selected sObject Record to the event attribute.  
                compEvent.setParams({"recordByUserEvent" : getSelectRecord });  
                // fire the event  
                compEvent.fire();
                //component.set("v.shareuserview", false);
            }
        }); 
        $A.enqueueAction(action); 
            }
    }
})