({
    getUserList : function(component, event, helper) {
        debugger;
        var Action = component.get("c.getUserprofile");
        component.set("v.userid",localStorage.getItem("UserSession"));
        var sessionname = localStorage.getItem("UserSession");
        Action.setParams({
            "username" : sessionname
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.TenantUser",results.Userlists);
                component.set("v.Users",results.userResults);
                component.set("v.ActivityLog",results.Activity_Log);
                component.set("v.Loginhistory",results.LoginHistory);
                component.set("v.Notification", results.Userlists.Enable_Notification__c);
                component.set("v.enableEmail", results.Userlists.Enable_Email__c);
                component.set("v.superAdmin",results.isAdmin);
                if(results.usernotificationLists.length > 0){  
                    for (var i = 0; i < results.usernotificationLists.length; i++) { 
                        if(results.usernotificationLists[i].Name == 'Create Transaction'){
                            component.set("v.CreateTransaction",results.usernotificationLists[i].Notification__c);
                            component.set("v.CreateTransactionEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Validate Transaction'){
                            component.set("v.ValidateTransaction",results.usernotificationLists[i].Notification__c);
                            component.set("v.ValidateTransactionEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Approve Transaction'){
                            component.set("v.ApproveTransaction",results.usernotificationLists[i].Notification__c);
                            component.set("v.ApproveTransactionEmail",results.usernotificationLists[i].Email__c);
                        }                       
                        if(results.usernotificationLists[i].Name == 'Request for Information'){
                            component.set("v.RequestforInformation",results.usernotificationLists[i].Notification__c);
                            component.set("v.RequestforInformationEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Request For Quote'){
                            component.set("v.RequestforQuote",results.usernotificationLists[i].Notification__c);
                            component.set("v.RequestforQuoteEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Submission'){
                            component.set("v.BidSubmission",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidSubmissionEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Accept'){
                            component.set("v.BidAccept",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidAcceptEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Reject'){
                            component.set("v.BidReject",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidRejectEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Closed'){
                            component.set("v.BidClosed",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidClosedEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Bid Inactive'){
                            component.set("v.BidInactivate",results.usernotificationLists[i].Notification__c);
                            component.set("v.BidInactivateEmail",results.usernotificationLists[i].Email__c);
                        }
                        if(results.usernotificationLists[i].Name == 'Execute Agreement'){
                            component.set("v.ExecuteAgreement",results.usernotificationLists[i].Notification__c);
                            component.set("v.ExecuteAgreementEmail",results.usernotificationLists[i].Email__c);
                        }
                        
                        
                    }
                }
            }
        });
        $A.enqueueAction(Action);
    },
    
    saveUserprofile : function(component, event, helper) {
        debugger; 
        var errorFlag = 'false';
        var firstname = component.find("firstnameVal").get("v.value");
        var lastname = component.find("lastnameVal").get("v.value");
        var phone = component.find("phoneval").get("v.value"); 
        component.set("v.firstnameError",'');
        component.set("v.lastnameError",'');
        component.set("v.phoneError",'');
        var regPhoneNo = /^[0-9]*$/;
        if(firstname == undefined || firstname == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.firstnameError",'Input field required');
        }
        if(lastname == undefined || lastname == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.lastnameError",'Input field required');
        }
        if(!$A.util.isEmpty(phone))
        {   
            
            if(!phone.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.phoneerror", 'Please enter valid phone number'); 
            } 
            else if(phone.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.phoneerror", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.phoneerror", ''); 
                }
        }
        
          if(errorFlag == 'false') {  
                var Action1 = component.get("c.saveprofile");
                var sessionname = localStorage.getItem("UserSession");
                Action1.setParams({
                    "username" : sessionname ,
                    "tenant" : component.get("v.TenantUser"),
                    "orgId" : '',
                    "tenantId" :''
                });
                Action1.setCallback(this, function(actionResult1) {
                    debugger;
                    var state = actionResult1.getState(); 
                    var results = actionResult1.getReturnValue();
                    if(state === "SUCCESS"){
                        
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': 'Profile Updated Successfully',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                });
                $A.enqueueAction(Action1);
            }
        
    },
    updatePassword : function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.oldpasswordvalidation",false);
        var oldPassword = component.find("oldPassword").get("v.value");
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value"); 
        var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
        component.set("v.currentpassworderror",'');
        component.set("v.newpassworderror",'');
        component.set("v.confirmpassworderror",'');
        //var re = new RegExp("^([a-z0-9]{5,})$");   
        if(oldPassword == undefined || oldPassword == ""){    
            errorFlag = 'true';
            component.set("v.requiredvalidation", true);
            component.set("v.currentpassworderror",'Input field required');
        }else{
            component.set("v.currentpassworderror",'');
        }
        if(newPassword == undefined || newPassword == ""){
            errorFlag = 'true';
            component.set("v.requiredvalidation", true);
            component.set("v.newpassworderror",'Input field required');
        }else{
            component.set("v.newpassworderror",'');
        }
        if(confirmPassword == undefined || confirmPassword == ""){
            errorFlag = 'true';
            component.set("v.requiredvalidation", true);
            component.set("v.confirmpassworderror",'Input field required');
        }
        else if(newPassword != confirmPassword){  
            errorFlag = 'true';
            component.set("v.confirmpassworderror",'');
            component.set("v.changepasswordvalidation",true);
            component.set("v.requiredvalidation", false);
            component.set("v.combinationvalidation",false);
        }
            else if (!re.test(confirmPassword)) {
                errorFlag = 'true';
                component.set("v.combinationvalidation",true);
                component.set("v.changepasswordvalidation",false);
                component.set("v.requiredvalidation",false);
            }
        
        if(errorFlag == 'false') {  
            component.set("v.changepasswordvalidation",false);
            component.set("v.combinationvalidation",false);
            component.set("v.requiredvalidation",false);
            var sessionname = localStorage.getItem("UserSession");
            var Action1 = component.get("c.changePassword");
            
            Action1.setParams({
                "username" : sessionname,
                "oldpassword": oldPassword,
                "newpassword": newPassword
            });
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    if(results == 'Success'){
                        
                        var showToast = $A.get('e.force:showToast');
                        showToast.setParams(
                            {
                                'title': 'Success: ',
                                'message': 'Password Successfully Changed',
                                'type': 'Success'
                            }
                        );
                        showToast.fire();
                        $A.get('e.force:refreshView').fire();
                    }else{
                        component.set("v.oldpasswordvalidation",true);
                    }
                    
                }
            });
            $A.enqueueAction(Action1);
        }
    },
    
    updateNotification: function(component, event, helper) {
        debugger;
        var checkNotification = component.find("chkboxNotification").get("v.value");
        var checkEmail = component.find("chkboxEmail").get("v.value");
        var sessionname = localStorage.getItem("UserSession");
        var Action1 = component.get("c.changeNotification");
        
        Action1.setParams({
            "username" : sessionname,
            "notification": checkNotification,
            "email": checkEmail
        });
        Action1.setCallback(this, function(actionResult1) {
            debugger;
            var state = actionResult1.getState(); 
            var results = actionResult1.getReturnValue();
            if(state === "SUCCESS"){
                var site = $A.get("{!$Label.c.Org_URL}");
                window.location = '/'+site+'/s/dashboard';
            }
        });
        $A.enqueueAction(Action1);
        
    },    
  
    updateMynotifications: function(component, event, helper) {
        debugger;
        var notificationIds=new Array();
        var ctN = component.find("chkboxcreateTransaction").get("v.value");
        var ctE = component.find("chkboxcreateTransactionemail").get("v.value");
        notificationIds.push("Create Transaction:"+ctN+":"+ctE);
        var vtN = component.find("chkboxvalidateTransaction").get("v.value");
        var vtE = component.find("chkboxvalidateTransactionemail").get("v.value");
        notificationIds.push("Validate Transaction:"+vtN+":"+vtE);
        var atN = component.find("chkboxapproveTransaction").get("v.value");
        var atE = component.find("chkboxapproveTransactionemail").get("v.value");
        notificationIds.push("Approve Transaction:"+atN+":"+atE);
        var rtN = component.find("chkboxrequestTransaction").get("v.value");
        var rtE = component.find("chkboxrequestTransactionemail").get("v.value");
        notificationIds.push("Request for Information:"+rtN+":"+rtE);
        var rqN = component.find("chkboxrequestQuote").get("v.value");
        var rqE = component.find("chkboxrequestQuoteemail").get("v.value");
        notificationIds.push("Request For Quote:"+rqN+":"+rqE);
        var bsN = component.find("chkboxbidsubmission").get("v.value");
        var bsE = component.find("chkboxbidsubmissionemail").get("v.value");
        notificationIds.push("Bid Submission:"+bsN+":"+bsE);
        var baN = component.find("chkboxbidAccept").get("v.value");
        var baE = component.find("chkboxbidAcceptemail").get("v.value");
        notificationIds.push("Bid Accept:"+baN+":"+baE);
        var brN = component.find("chkboxbidReject").get("v.value");
        var brE = component.find("chkboxbidRejectemail").get("v.value");
        notificationIds.push("Bid Reject:"+brN+":"+brE);
        var bcN = component.find("chkboxbidClosed").get("v.value");
        var bcE = component.find("chkboxbidClosedemail").get("v.value");
        notificationIds.push("Bid Closed:"+bcN+":"+bcE);
        var biN = component.find("chkboxbidInactive").get("v.value");
        var biE = component.find("chkboxbidInactiveemail").get("v.value");
        notificationIds.push("Bid Inactive:"+biN+":"+biE);
        var eaN = component.find("chkboxexeAgreement").get("v.value");
        var eaE = component.find("chkboxexeAgreementemail").get("v.value");
        notificationIds.push("Execute Agreement:"+eaN+":"+eaE);
        var idListJSON=JSON.stringify(notificationIds);
        // alert(idListJSON);
        var action = component.get("c.changeMynotifications");
        var sessionname = localStorage.getItem("UserSession");
        action.setParams({
            "username" : sessionname,
            "listnotify":idListJSON
        });
        action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){               
                
                var showToast = $A.get('e.force:showToast');
                showToast.setParams(
                    {
                        'title': 'Success: ',
                        'message': 'Notification Updated Successfully',
                        'type': 'Success'
                    }
                );
                showToast.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    }
})